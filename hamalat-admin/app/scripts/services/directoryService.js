hamalatAdminApp
  .factory('directoryService',['$http','$rootScope', function($http,$rootScope) {

    return{
      addDirectory : function(info) {
        return $http.post($rootScope.domain+'HamlaDalel/edit', info);
      },

      showDirectory : function(campaignID) {
        return $http.post($rootScope.domain+'HamlaDalel/Index', campaignID);
      }
    };

  }]);
