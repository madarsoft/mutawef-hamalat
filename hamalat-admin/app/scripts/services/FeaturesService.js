hamalatAdminApp
  .factory('FeaturesService',['$http','$rootScope', function($http, $rootScope) {

    return{
      addParagraph : function(info) {
        return $http.post($rootScope.domain+'HamlaAdvantages/create', info);
      } ,

      updateParagraph : function(info) {
        return $http.post($rootScope.domain+'HamlaAdvantages/Edit', info);

      },
      removeParagraph : function(paragraphId){
        return $http.post($rootScope.domain+'HamlaAdvantages/delete', paragraphId);
      },
      viewFeatures : function(info){
        return $http.post($rootScope.domain+'HamlaAdvantages/Index', info);
      }
    };



  }]);
