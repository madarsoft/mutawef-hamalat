'use strict';

/**
 * @ngdoc function
 * @name hamalatAdminApp.controller:LoginCtrl
 * @description
 * # LoginCtrl
 * Controller of the hamalatAdminApp
 */
hamalatAdminApp
  .controller('competitionsCtrl', ['$scope', '$rootScope', '$http', '$routeParams','$location','CompetitionsService',
    function ($scope,$rootScope, $http,  $routeParams,$location, CompetitionsService) {


      $rootScope.topTitle = 'حملتي';
      $rootScope.logOut= false;
      $rootScope.loading= false;
      $rootScope.backBtn= true;
      $rootScope.message = '';

      var user = $rootScope.user ;
      $scope.myDayIndex = 0;
      var sliderOpen= 0;

      if(user != ''){
         $scope.hamlaId = user.hamlaId;

      }


      $scope.days =[
        {id:'1', name:'ما قبل التروية', loaded :false},
        {id:'2', name:'الثامن من ذى الحجة)التروية)', loaded :false},
        {id:'3', name:'يوم عرفة)التاسع من ذى الحجة)', loaded :false},
        {id:'4', name:'يوم النحر)العاشر من ذى الحجة)', loaded :false},
        {id:'5', name:'أول أيام التشريق', loaded :false},
        {id:'6', name:'ثانى أيام التشريق', loaded :false},
        {id:'7', name:'ثالث أيام التشريق', loaded :false},
        {id:'8', name:'اليوم الأخير', loaded :false}
      ];

      $scope.competitionsTable = [{day:[]}, {day:[]},  {day:[]} , {day:[]} ,{day:[]}, {day:[]},{day:[]} ,{day:[]}];

      $scope.daySchedule = [];
      $scope.editeText ='';
      $scope.inputParagraph = {
        text:''
      };
      $scope.removeIndex = 0;
      ///////////////////////////////////////////////////////////////



      var id= 0;
      $scope.loadDay = function() {



        $('header h3').html($rootScope.mainTitle )


        for (var i = id; i < (($scope.myDayIndex+1) *10); i++) {
          var p = {
            id: i + 1,
            text: 'paragraph' + (i + 1),
            edit: false
          }

          $scope.$apply(function(){
            $scope.days[$scope.myDayIndex].loaded = true;
            $scope.competitionsTable[$scope.myDayIndex].day.push(p);
          })


        }

        id= ($scope.myDayIndex+1)  * 10;

        console.log($scope.competitionsTable)

      }

      /////////////////////////////////////////////////////////////
      $rootScope.backAction = function () {
        window.location.href ='#/dashboard';
      }


      $scope.getDailyCompetitions= function () {
        $scope.$apply(function(){
          $rootScope.mainTitle =  $scope.days[$scope.myDayIndex].name;
        })

        if( $scope.days[$scope.myDayIndex].loaded == false){

          var info ={
            hamlaId :$scope.hamlaId,
            dayId :$scope.days[$scope.myDayIndex].id

          }
          $rootScope.loading= true;
          var res =  CompetitionsService.viewCompetitions(info)
          res.success(function (data) {
            $rootScope.loading= false;


            if (data.hasOwnProperty('result')) {
              if(data.result.hasOwnProperty('serverError'))
              {
                $rootScope.message= 'حدث خطأ اثناء الاتصال بالخادم .. من فضلك حاول مرة أخري';
                $('#failPopup').modal('show');
              }

            }

            else if(data.hasOwnProperty('hamlaCompetitions')){

              var list = data.hamlaCompetitions;
              for(var i = 0; i < list.length; i ++){

                  var temp = {
                    id:list[i].CompetitionId,
                    text:list[i].CompetitionsQuestion,
                    edit:false
                  }
                  $scope.competitionsTable[$scope.myDayIndex].day.push(temp);
                  $scope.days[$scope.myDayIndex].loaded = true;


              }

            }
          });
          res.error(function (err) {
            $rootScope.loading= false;
            $rootScope.message = 'حدث خطأ اثناء الاتصال بالسيرفر .. من فضلك حاول مرة أخري';

            $('#failPopup').modal('show');
          });
        }




      }

      $scope.changeToEditMode= function (index){

        $scope.closeOtherEdits(index);
        $scope.competitionsTable[$scope.myDayIndex].day[index].edit= true;
        $scope.editeText =$scope.competitionsTable[$scope.myDayIndex].day[index].text ;
        $scope.competitionsTable[$scope.myDayIndex].day[index].text =  $scope.competitionsTable[$scope.myDayIndex].day[index].text.replace(/<br\s*\/?>/mg,"\n");


        $('.stepInput textarea').height('95%')
        $('.stepInput textarea').css('min-height', '150px');

      }

      $scope.closeOtherEdits = function(index){

        for(var i =0; i < $scope.competitionsTable.length ; i ++){
          for(var j = 0; j< $scope.competitionsTable[i].day.length; j++)

            $scope.competitionsTable[i].day[j].edit = false;

        }
      }


      $scope.cancelEdit= function (index){

        $scope.competitionsTable[$scope.myDayIndex].day[index].text = $scope.editeText;

        $scope.competitionsTable[$scope.myDayIndex].day[index].edit= false;
      }




      $scope.addParagraph= function (index){


        var temp = {
          hamlaId :$scope.hamlaId,
          dayId :$scope.days[$scope.myDayIndex].id,
          text:$scope.inputParagraph.text.replace(/\r?\n/g,'<br/>')
        }


        $rootScope.loading= true;
        var res =  CompetitionsService.addParagraph(temp)
        res.success(function (data) {
          $rootScope.loading= false;



          if(data.hasOwnProperty('competitionId')){
            var temp = {
              id:data.competitionId,
              text:$scope.inputParagraph.text.replace(/\r?\n/g,'<br/>'),
              edit:false
            }
            $scope.competitionsTable[$scope.myDayIndex].day.push(temp);
            $scope.inputParagraph.text = '';

            $rootScope.message= 'تم اضافة ســــؤال بنجاح';
            $('#successPopup').modal('show');

           console.log( $scope.competitionsTable)
          }else{
            $rootScope.message= 'حدث خطأ اثناء الاتصال بالخادم .. من فضلك حاول مرة أخري';
            $('#failPopup').modal('show');
          }



        });
        res.error(function (err) {
          $rootScope.loading= false;
          $rootScope.message = 'حدث خطأ اثناء الاتصال بالسيرفر .. من فضلك حاول مرة أخري';

          $('#failPopup').modal('show');
        });

      }


      $scope.saveEdit= function (index){

        var currentParagraph =  $scope.competitionsTable[$scope.myDayIndex].day[index];
        var info ={
          compititionId :(currentParagraph.id).toString(),
          text :currentParagraph.text.replace(/\r?\n/g,'<br/>')

        }

        $rootScope.loading= true;
        var res =  CompetitionsService.updateParagraph(info)
        res.success(function (data) {
          $rootScope.loading= false;


          if (data.hasOwnProperty('result')) {
            if(data.result.hasOwnProperty('serverError'))
            {
              $rootScope.message= 'حدث خطأ اثناء الاتصال بالخادم .. من فضلك حاول مرة أخري';
              $('#failPopup').modal('show');
            }
            else if(data.result == true){
              $scope.competitionsTable[$scope.myDayIndex].day[index].text = $scope.competitionsTable[$scope.myDayIndex].day[index].text.replace(/\r?\n/g,'<br/>');
              $scope.competitionsTable[$scope.myDayIndex].day[index].edit= false;

              $rootScope.message= 'تم حفظ التعديلات بنجاح';
              $('#successPopup').modal('show');
            }else{
              $rootScope.message= 'حدث خطأ اثناء الاتصال بالخادم .. من فضلك حاول مرة أخري';
              $('#failPopup').modal('show');
            }

          }
        });
        res.error(function (err) {
          $rootScope.loading= false;
          $rootScope.message = 'حدث خطأ اثناء الاتصال بالسيرفر .. من فضلك حاول مرة أخري';

          $('#failPopup').modal('show');
        });

      }
      $scope.confirmRemoveParagraph= function (index){

        $rootScope.removeIndex = index;
        $rootScope.message = 'هل تريد فعلا مســح الســـؤال';
        $('#dialogPopup').modal('show');

      }

      $scope.removeParagraph= function (){
        var index =$rootScope.removeIndex;
        var id= {
          compititionId:( $scope.competitionsTable[$scope.myDayIndex].day[index].id).toString()
        }
        console.log(id.compititionId)
        $rootScope.loading= true;
        var res =  CompetitionsService.removeParagraph(id)
        res.success(function (data) {
          $rootScope.loading= false;


          if (data.hasOwnProperty('result')) {
            if(data.result.hasOwnProperty('serverError'))
            {
              $rootScope.message= 'حدث خطأ اثناء الاتصال بالخادم .. من فضلك حاول مرة أخري';
              $('#failPopup').modal('show');
            }
            else if(data.result == true){

              $scope.competitionsTable[$scope.myDayIndex].day.splice(index, 1);
              $rootScope.message= 'تم مسح الســــؤال  بنجاح';
              $('#successPopup').modal('show');

            }else{
              $rootScope.message= 'حدث خطأ اثناء الاتصال بالخادم .. من فضلك حاول مرة أخري';
              $('#failPopup').modal('show');
            }

          }
        });
        res.error(function (err) {
          $rootScope.loading= false;
          $rootScope.message = 'حدث خطأ اثناء الاتصال بالسيرفر .. من فضلك حاول مرة أخري';

          $('#failPopup').modal('show');
        });
      }


      var day ;
      var sliderOpen = 0
      setTimeout(function () {initSlick()}, 100);
      function initSlick(){

        if(sliderOpen == 0) {


          $('.schedule').slick({
            rtl: true,
            infinite: false,
            speed: 600,
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: true,
            dots: true,
            prevArrow:"<img class='rightArr' src='../images/rightArrow.png'>",
            nextArrow:"<img class='leftArr' src='../images/leftArrow.png'>"
          });


          var currentSlideGlobal = 0;
          var nextSlideGlobal = 0;
          $('.schedule').on('beforeChange', function (event, slick, currentSlide, nextSlide) {

            currentSlideGlobal = currentSlide;
            nextSlideGlobal = nextSlide;


          });
          $('.schedule').on('afterChange', function (event, slick, currentSlide) {

            //alert('eeee')
            var currentDay = nextSlideGlobal;
            var day = nextSlideGlobal + 1;

            console.log('afterChange' + day);
            $scope.myDayIndex = currentDay;
            if($scope.myDayIndex == 0){
              $('.rightArr').hide();
              $('.leftArr').show();
            }else if($scope.myDayIndex == 7){
              $('.leftArr').hide();
              $('.rightArr').show();
            }else{
              $('.leftArr').show();
              $('.rightArr').show();
            }
            if(currentSlideGlobal != nextSlideGlobal) {
              $('html,body').animate({scrollTop: $('.firstInput').offset().top - ($('.firstInput').height() * 2)}, 200);
            }
            $scope.getDailyCompetitions();

          });

        }

        sliderOpen = 1;
        $('.schedule').slick('slickGoTo', parseInt($rootScope.realDay - 1))
      }


      $rootScope.cancelDialogPopup = function () {

        $('#dialogPopup').modal('hide');
        $scope.removeIndex = 0;
      }

      $rootScope.okDialogPopup = function () {

        $('#dialogPopup').modal('hide');
        $scope.removeParagraph();
      }

    }]);
