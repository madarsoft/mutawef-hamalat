'use strict';

/**
 * @ngdoc function
 * @name hamalatAdminApp.controller:LoginCtrl
 * @description
 * # LoginCtrl
 * Controller of the hamalatAdminApp
 */
hamalatAdminApp
  .controller('resetPassword', ['$scope', '$rootScope', '$http', '$routeParams','$location','User','UserService',
    function ($scope,$rootScope, $http,  $routeParams,$location, User, UserService) {

      $rootScope.loading= false;
      $rootScope.backBtn= false;
      $rootScope.logOut= false;
      $scope.userPassword = '';
      $scope.userRePassword = '';
      $rootScope.mainTitle = 'تغير كلمة المرور';
      $scope.newPassword='';
      $rootScope.domain = 'http://api.mutawef.com/';
      $scope.guid = $routeParams.guid;
      $rootScope.topTitle = 'حملتي';


     $scope.resetUserPassword = function (isValid) {

       if(isValid) {
         if ($scope.userPassword == $scope.userRePassword) {

           $scope.newPassword=$scope.userPassword;


           var info = {
             password:$scope.newPassword,
             guid:$scope.guid
           }
           $rootScope.loading= true;
           var res = UserService.resetMyPassword(info);

           res.success(function (data, status, headers, config) {
             $rootScope.loading= false;
             if(data.hasOwnProperty('serverError'))
             {
               $rootScope.message= 'حدث خطأ اثناء الاتصال بالخادم .. من فضلك حاول مرة أخري';
               $('#failPopup').modal('show');
             }
             else if (data.hasOwnProperty('result')) {

               if (data.result == true) {



                 $rootScope.message = 'تم تغير كلمة المرور';
                 $('#successPopup').modal('show');
               } else {
                 $rootScope.message = 'لا يمكنك تغير كلمة المرور في حاليا , جرب نسيت كلمة المرور مرة أخري';
                 $('#failPopup').modal('show');
               }
             }
           });


           res.error(function (data, status, headers, config) {
             $rootScope.loading= false;
             $rootScope.message = 'حدث خطأ اثناء الاتصال بالسيرفر .. من فضلك حاول مرة أخري';
             $('#failPopup').modal('show');

           });
         }else{

           $rootScope.message ='يجب أن يتطابق كلا من كلمتي المرور';
           $('#failPopup').modal('show');

         }
       }

      };


    }]);
