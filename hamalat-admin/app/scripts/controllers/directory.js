'use strict';

/**
 * @ngdoc function
 * @name hamalatAdminApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the hamalatAdminApp
 */



hamalatAdminApp
  .controller('directory', ['$scope', '$rootScope', '$http', '$routeParams','$location','directoryService',
    function ($scope, $rootScope, $http, $routeParams, $location, directoryService) {

      $rootScope.topTitle = 'حملتي';
      $rootScope.logOut= false;
      $rootScope.loading= false;
      $rootScope.backBtn= true;
      $rootScope.mainTitle = 'تعديل الدليـــل';
      // add object to fill data in

      $scope.directoryList = $rootScope.directoryData;

      var user = $rootScope.user ;



      if(user != ''){
         $scope.hamlaId = user.hamlaId;

      }

      $rootScope.backAction = function () {
        window.location.href ='#/showDirectory';
      };
      $scope.saveDirectory = function (isValid) {

        var info = {
          hamlaId:$scope.hamlaId ,
          managementPhone:$scope.directoryList[0].guidPhone,
          doctorPhone:$scope.directoryList[1].guidPhone,
          muftiPhone:$scope.directoryList[2].guidPhone,
          CulturalPhone:$scope.directoryList[3].guidPhone,
          lostPhone:$scope.directoryList[4].guidPhone
        };
        if(isValid){
          $rootScope.loading= true;
          var res = directoryService.addDirectory(info);

          res.success(function (data) {
            $rootScope.loading= false;
            if (data.hasOwnProperty('result')) {
              if(data.result.hasOwnProperty('serverError'))
              {
                $rootScope.message= 'حدث خطأ اثناء الاتصال بالخادم .. من فضلك حاول مرة أخري';
                $('#failPopup').modal('show');
              }
              else if(data.result == true){
                //window.location.href = "#/showDirectory";
                $rootScope.message= 'تم حفظ التعديلات بنجاح';
                //$('#successPopup').modal('show');
                $('#successOkOnly').modal({
                  backdrop: 'static',
                  keyboard: false
                }).modal('show');
              }else{
                $rootScope.message= 'حدث خطأ اثناء الاتصال بالخادم .. من فضلك حاول مرة أخري';
                $('#failPopup').modal('show');
              }
            }
          });

          res.error(function (err) {
            $rootScope.loading= false;
            $rootScope.message= 'حدث خطأ اثناء الاتصال بالخادم .. من فضلك حاول مرة أخري';
            $('#failPopup').modal('show');
          });
        }
      };
      $rootScope.okOnPopup = function () {
        window.location.href = "#/showDirectory";
      }

      $scope.updateDirectory =  function () {
        window.location.href = "#/showDirectory";
      };

    }]);
