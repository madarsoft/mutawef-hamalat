'use strict';

/**
 * @ngdoc overview
 * @name hamalatAdminApp
 * @description
 * # hamalatAdminApp
 *
 * Main module of the application.
 */
var hamalatAdminApp = angular
  .module('hamalatAdminApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch'
  ])
  .config(function ($routeProvider, $httpProvider) {
    $routeProvider
      .when('/main', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl',
        controllerAs: 'main'
      })
      .when('/directory', {
        templateUrl: 'views/directory.html',
        controller: 'directory'
      })
      .when('/showDirectory', {
        templateUrl: 'views/showDirectory.html',
        controller: 'showDirectory'
      })
      .when('/login', {
        templateUrl: 'views/LoginView.html',
        controller: 'LoginCtrl',
        controllerAs: 'login'
      })
      .when('/forgotMyPassword', {
        templateUrl: 'views/forgotPassword.html',
        controller: 'forgotPassword'
      })
      .when('/resetMyPassword', {
        templateUrl: 'views/resetPassword.html',
        controller: 'resetPassword'
      })
      .when('/register', {
        templateUrl: 'views/register.html',
        controller: 'RegisterCtrl',
        controllerAs: 'register'
      })
      .when('/editProfile', {
        templateUrl: 'views/editProfile.html',
        controller: 'editProfile'
      })
      .when('/profile', {
        templateUrl: 'views/profile.html',
        controller: 'profile'
      })
      .when('/dashboard', {
        templateUrl: 'views/dashboard.html',
        controller: 'dashboardCtrl',
        controllerAs: 'dashboard'
      })
      .when('/schedule', {
        templateUrl: 'views/schedule.html',
        controller: 'scheduleCtrl',
        controllerAs: 'schedule'
      })
      .when('/addSchedule', {
        templateUrl: 'views/addSchedule.html',
        controller: 'add-scheduleCtrl',
        controllerAs: 'add-schedule'
      })
      .when('/scheduleDetails', {
        templateUrl: 'views/scheduleDetails.html',
        controller: 'scheduleDetailsCtrl',
        controllerAs: 'scheduleDetails'
      })
      .when('/competitions', {
        templateUrl: 'views/competitions.html',
        controller: 'competitionsCtrl',
        controllerAs: 'competitions'
      })
      .when('/news', {
        templateUrl: 'views/news.html',
        controller: 'newsCtrl',
        controllerAs: 'news'
      })
      .when('/features', {
        templateUrl: 'views/features.html',
        controller: 'featuresCtrl',
        controllerAs: 'features'
      })
      .when('/faq', {
        templateUrl: 'views/faq.html',
        controller: 'faqCtr',
        controllerAs: 'faq'
      })
    $httpProvider.defaults.headers.common = {};
    $httpProvider.defaults.headers.post = {};
    $httpProvider.defaults.headers.put = {};
    $httpProvider.defaults.headers.patch = {};
      //.otherwise({
      //  redirectTo: '/'
      //});
  })




//http://stackoverflow.com/questions/14712223/how-to-handle-anchor-hash-linking-in-angularjs
.run(function($rootScope, $location, $anchorScroll, $routeParams) {
  //when the route is changed scroll to the proper element.
  $rootScope.$on('$routeChangeSuccess', function(newRoute, oldRoute) {
    $location.hash($routeParams.scrollTo);
    $anchorScroll();
  });
});
