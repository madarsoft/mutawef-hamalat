function doaaView (){

    this.doaaController = new doaaController();
}


doaaView.prototype.showDoaa=function (DoaaList){
   var DoaaListLength = DoaaList.length;    
    for (var i = 0 ; i < DoaaListLength ; i++){    
    $('#doaas').append("<div class='doaa2'><p class='mansak'>"+DoaaList[i].text+"</p><p class='number doaaSource'>"+DoaaList[i].source+"</p></div>")    
             this.initializeFontSize();
    }
}

doaaView.prototype.initializeFontSize=function(){    
    
var initialFontSize = parseInt(this.getFontSize());

 $('.mansak').css('font-size', initialFontSize);
$('.number').css('font-size', initialFontSize);   
$('.sub_title').css('font-size', initialFontSize + 2 );
$('.main_title').css('font-size', initialFontSize + 4);
$('.doaa2').css('font-size', initialFontSize);

   

}

doaaView.prototype.getFontSize=function(){
var fontSize = localStorage.getItem('fontSize');
    return (fontSize);
}

doaaView.prototype.setFontSize=function(newFontSize){
localStorage.setItem('fontSize',newFontSize);
}



doaaView.prototype.inc=function (){	
var myobj = this;    
var oldFontSize =myobj.getFontSize(); 
    var f1 = parseInt(oldFontSize) + 2
  if(f1>22){
    f1 = 22;
    
    }    
myobj.setFontSize(f1);
		  if(f1<=22){
		
				$('.mansak').css('font-size', f1);
				$('.number').css('font-size', f1);
		  }


        
			var f2= parseInt($('.sub_title').css('font-size')) + 2;
			if(f2<=24){
			
				$('.sub_title').css('font-size', f2);
			}
			  
			var f3= parseInt($('.main_title').css('font-size')) + 2;
			if(f3<=26){
			
				$('.main_title').css('font-size', f3);
			}


}


doaaView.prototype.dec=function (){	

    
    var myobj = this;    
var oldFontSize =myobj.getFontSize(); 
    var f1 = parseInt(oldFontSize - 2)
    if(f1<8){
    f1 = 8;
    
    }
    
myobj.setFontSize(f1);
	  if(f1>=12){
	
			$('.mansak').css('font-size', f1);
			$('.number').css('font-size', f1);
	  }
	  
	   var  f2= parseInt($('.sub_title').css('font-size')) - 2;
	  if(f2>=14){
	
			$('.sub_title').css('font-size', f2);
	  }
	  
	  var  f3= parseInt($('.main_title').css('font-size')) - 2;
	  if(f3>=16){
	
			$('.main_title').css('font-size', f3);
	  }
}


    

$(function(){
    
    var doaa_view = new doaaView();
    if (localStorage.getItem('fontSize')==null){
    localStorage.setItem('fontSize',14);
    }
    

	$('#name3').click(function(e) {
		 $('#doaas').html('');
                
         doaa_view.doaaController.retrieveFromXML();
        
        doaa_view.initializeFontSize(); 
    });
	
	
	$('.increase').click(function(e) {
         doaa_view.inc();
    });
	
	$('.decrease').click(function(e) {
         doaa_view.dec();
    });
	
    
});