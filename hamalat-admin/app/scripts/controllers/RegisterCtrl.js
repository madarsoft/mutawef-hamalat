'use strict';

/**
 * @ngdoc function
 * @name hamalatAdminApp.controller:LoginCtrl
 * @description
 * # LoginCtrl
 * Controller of the hamalatAdminApp
 */
hamalatAdminApp
  .controller('RegisterCtrl', ['$scope', '$rootScope', '$http', '$routeParams','$location','User','UserService',
    function ($scope,$rootScope, $http,  $routeParams,$location, User,UserService) {

      $rootScope.loading= false;
      $rootScope.backBtn= true;
      $rootScope.logOut= false;
      $rootScope.mainTitle = 'اضافة حملة' ;
      $rootScope.topTitle = 'حملتي';

      $rootScope.message = '';

      $scope.info={
        campaignName:'',
        userName:'',
        userEmail:'',
        userPassword:'',
        userRePassword:'',
        telephone:'',
        lat:'',
        lng:''
      };

      $rootScope.backAction = function () {
        window.location.href ='#/login';
      }
      $scope.validateEmail = function () {
        $('#emailContainer').removeClass('hasError');
        $('#emailContainer').removeClass('noError');
        var myMail= {
          email:$scope.info.userEmail
        }
        var res = UserService.validateEmail(myMail);

        res.success(function (data) {


          if(data.hasOwnProperty('serverError'))
          {
            $('#emailContainer').addClass('hasError');
            $rootScope.message= 'حدث خطأ اثناء الاتصال بالخادم .. من فضلك حاول مرة أخري';
            $('#failPopup').modal('show');
          }
          else if (data.hasOwnProperty('result')) {

            if (data.result == true) {
              $('#emailContainer').addClass('hasError');

              $rootScope.message = 'من فضللك ادخل بريد إلكتروني غير مسجل من قبل';
              $('#successPopup').modal('show');
            } else {
              $('#emailContainer').addClass('noError');
            }
          }

        });

        res.error(function (err) {
          $('#emailContainer').addClass('hasError');
          $rootScope.message = 'حدث خطأ اثناء الاتصال بالسيرفر .. من فضلك حاول مرة أخري';
          $('#failPopup').modal('show');
        });
      }
      $scope.addHamla = function (isValid) {

        if(isValid) {
          if ($scope.info.userPassword == $scope.info.userRePassword) {

            var userData = {
              hamlaName: $scope.info.campaignName,
              userName: $scope.info.userName,
              email: $scope.info.userEmail,
              password: $scope.info.userPassword,
              phone: $scope.info.telephone,
              lat:$scope.info.lat,
              lng:$scope.info.lng
            };
            $rootScope.loading= true;
            var res = UserService.register(userData);

            res.success(function (data) {

              $rootScope.loading= false;
              if (data.hasOwnProperty('serverError')) {
                $rootScope.message = 'حدث خطأ اثناء الاتصال بالسيرفر .. من فضلك حاول مرة أخري';
                $('#failPopup').modal('show');
              }
              else if (data.hasOwnProperty('userInfo')) {
                if(data.userInfo == 'duplicate email '){
                  $rootScope.message = 'من فضللك ادخل بريد إلكتروني غير مسجل من قبل';
                  $('#failPopup').modal('show');
                }
                else{
                  $rootScope.message = 'تم التسجيل بنجاج, و سيصلك الرقم الخاص  بالحملة علي البريد الإلكتروني';
                  $('#successOkOnly').modal({
                    backdrop: 'static',
                    keyboard: false
                  }).modal('show');
                }
              }

            });

            res.error(function (err) {
              $rootScope.loading= false;
              $rootScope.message = 'حدث خطأ اثناء الاتصال بالسيرفر .. من فضلك حاول مرة أخري';
              $('#failPopup').modal('show');
            });
          }
          else{

            $rootScope.message ='يجب أن يتطابق كلا من كلمتي المرور';
            $('#failPopup').modal('show');
          }
        }

      };


      $rootScope.okOnPopup = function () {
        window.location.href = "#/login";
      }




    }]);
