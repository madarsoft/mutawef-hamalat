
var lat = 180;
var lon = 180;
var hamla_id = null;
var hamla_name = '';
var device_id = null;
var day = 1;
var realDay = 0;

var sliderOpen =0;
var sliderOpen2 =0;

var Director = null;
var doctor = null;
var technical = null;
var lost = null;
var culture = null;
var hamalaDays = new Array();
var tasks = new Array();
var phones = new Array();
// var locations = new Array();
var AllLocations;
var currentDay= 0;

function loading() {
    hamla_id = localStorage.getItem('Reg_hamala');
    hamla_name= localStorage.getItem('Reg_hamalaName');
    // hamla_id='3'
    device_id = localStorage.getItem('device_id');

    day = localStorage.getItem('current_day');


    realDay = localStorage.getItem('current_day');

    // alert(day)

    localStorage.setItem('task_timestamp', 0);


    get_hamla_days();
    $('.dashboardItem').click(function () {
        var id = $(this).attr('id');
        if (id == 'myData') {
            $('.dashboardWrapper').fadeOut();
            localStorage.setItem('new_update', 2);
            window.location.href = 'registration.html';
        }
        else {
            if(id == 'table'){
                $('header h3').html(hamalaDays[day-1]);
                $('.allFavIcon').show();

                $('#tableWrapper').removeClass('activeWrapper slick-initialized slick-slider');
                $('#detectMeWrapper').removeClass('activeWrapper slick-initialized slick-slider');
                $('#detectMeWrapper').html('');
                $('#tableWrapper').html('');
                for(var i = 0 ; i < 8 ; i ++) {
                    var sliderItem = '<div class="activitiesOnSingleDay" id="slideDay' + i + '"></div>';
                    $('#tableWrapper').append(sliderItem);
                }

                sliderOptions();


            }
            else if (id == "detectMe") {
                $('header h3').html('المسابقــات');


                $('#tableWrapper').removeClass('activeWrapper slick-initialized slick-slider');
                $('#detectMeWrapper').removeClass('activeWrapper slick-initialized slick-slider');
                $('#detectMeWrapper').html('');
                $('#tableWrapper').html('');
                for(var i = 0 ; i < 8 ; i ++) {

                    var sliderItem2 = '<div class="activitiesOnSingleDay" id="slideDay2' + i + '"></div>';
                    $('#detectMeWrapper').append(sliderItem2);
                }


                competitionsSlider();
            }
            else if (id == 'daleel') {
                phone();
                $('header h3').html('الدليـــل');
            }else if(id == 'momayazat'){
                $('header h3').html('الممــيزات');
                advantages();
            }else if(id == 'notification'){
                $('header h3').html('الأخـبــار');
                notifications();
            }
            hideDashboard(id);
        }
    });


    $('#tableWrapper').addClass('activeWrapper');
    showDashboard();
    //if(day == 0){
    //
    //    $('#tableWrapper').addClass('activeWrapper');
    //    showDashboard();
    //
    //}else{
    //    $('.logOut').hide();
    //
    //    get_hamla_days();
    //    sliderOptions();
    //}

    //  notification_no();
    // map();

}


function get_hamla_days() {

    var days =[
        {id:'1', name:'ما قبل التروية', loaded :false},
        {id:'2', name:'الثامن من ذى الحجة)التروية)', loaded :false},
        {id:'3', name:'يوم عرفة)التاسع من ذى الحجة)', loaded :false},
        {id:'4', name:'يوم النحر)العاشر من ذى الحجة)', loaded :false},
        {id:'5', name:'أول أيام التشريق', loaded :false},
        {id:'6', name:'ثانى أيام التشريق', loaded :false},
        {id:'7', name:'ثالث أيام التشريق', loaded :false},
        {id:'8', name:'اليوم الأخير', loaded :false}
    ];
    var count = 0;
    $('#detectMeWrapper').html('');
    $('#tableWrapper').html('');


    for(var i = 0 ; i < days.length ; i ++)
    {
        hamalaDays.push(days[i].name);
        var sliderItem = '<div class="activitiesOnSingleDay" id="slideDay' + i + '"></div>';
        $('#tableWrapper').append(sliderItem);


    }

    AllLocations = new Array(days.length);
    $('#tableWrapper').addClass('activeWrapper');

    // sliderOptions();





}

function notification_no() {
    day = window.swipeyObj.day_counter;
    var task_timestamp = localStorage.getItem('task_timestamp');
    var notification_tstamp = localStorage.getItem('Time_stamp');


    $.ajax({

        url: 'http://204.187.101.75/mutawef2/API/Hamla_R_column.aspx?hamla_id=' + hamla_id + '&R_column=prog_day&Day_id=' + day + '&max_time_notif=' + notification_tstamp + '&max_time_activity=' + task_timestamp,
        dataType: 'xml',
        timeout: 300000,
        type: 'GET',
        success: function (data) {
            var total_number_notification = $(data).find("total_number_notification").text();
            $('.notify_num label').html(total_number_notification);

            var hamla_name = $(data).find("hamla_name").text();
            $('header h3').html(hamla_name);
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {

            alert("فشل الإتصال بالخادم , حاول مرة أخرى");
            // loadslidshow();
        }
    });
}

function schedular(myDay) {
    //day = window.swipeyObj.day_counter;
    // day =1;



    $('header h3').html(hamalaDays[myDay-1]);
    var task_timestamp = localStorage.getItem('task_timestamp');
    var notification_tstamp = localStorage.getItem('Time_stamp');




    //console.log('http://204.187.101.75/mutawef2/API/Hamla_R_column.aspx?hamla_id=' + hamla_id + '&R_column=prog_day&Day_id=' + myDay + '&max_time_notif=' + notification_tstamp + '&max_time_activity=' + task_timestamp)

    if($('#slideDay'+(myDay-1)).html() == '') {

        $('.loadingIcon').show();

        var info ={
            hamlaId :hamla_id,
            dayId :myDay.toString()

        }

        $.ajax({
            url:'http://api.mutawef.com/HamlaProgram/index',
            dataType:'json',
            type:'POST',
            data: JSON.stringify(info),
            success: function (data) {


                if (data.hasOwnProperty('result')) {
                    if(data.result.hasOwnProperty('serverError'))
                    {
                        $('.loadingIcon').hide();
                        alert("فشل الإتصال بالخادم , حاول مرة أخرى");
                    }

                }  else if(data.hasOwnProperty('hamlaPrograms')){

                    var list = data.hamlaPrograms;
                    $('.loadingIcon').hide();


                    var activityLen = list.length;
                    var count = 0;

                    var locations = new Array();
                    if(activityLen == 0){
                        AllLocations[(myDay-1)] = locations;
                        var item = ' <div class="tableActivity">'+
                            '<div class="activityIndex"><span>'+(0)+'</span></div>'+
                            '<div class="activityDetails">'+'لا يوجد جدول اليوم'+'</div>'+
                            '</div>';


                        $('#slideDay'+(myDay-1)).html('');
                        $('#slideDay'+(myDay-1)).append(item);
                    }else {


                        for (var i = 0; i < list.length; i++) {


                            var temp = {
                                id: list[i].programId,
                                text: list[i].programInfo,
                                edit: false
                            }
                            var item = ' <div class="tableActivity">'+
                                '<div class="activityIndex"><span>'+(i+1)+'</span></div>'+
                                '<div class="activityDetails">'+temp.text+'</div>'+
                                '</div>';

                            $('#slideDay'+(myDay-1)).append(item);
                            tasks.push(temp.id);

                        }
                    }

                }

            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                $('.loadingIcon').hide();
                alert("فشل الإتصال بالخادم , حاول مرة أخرى");
            }

        });
    }


}


function map() {

    $.ajax({


        url: 'http://204.187.101.75/mutawef2/API/Hamla_R_column.aspx?hamla_id=' + hamla_id + '&R_column=map',
        dataType: 'xml',
        timeout: 60000,
        type: 'GET',
        success: function (data) {
            $(data).find("map").each(function () {
                lat = $(this).find("Lat").text();
                lon = $(this).find("lon").text();

            });

        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {

            alert("فشل الإتصال بالخادم , حاول مرة أخرى");
            // loadslidshow();
        }
    });

}


function advantages() {


    $('#momayazatWrapper .advantages').html('')
    $('.loadingIcon').show();

    var info ={
        hamlaId :hamla_id

    }

    $.ajax({
        url:'http://api.mutawef.com/HamlaAdvantages/Index',
        dataType:'json',
        type:'POST',
        data: JSON.stringify(info),
        success: function (data) {


            if (data.hasOwnProperty('result')) {
                if(data.result.hasOwnProperty('serverError'))
                {
                    $('.loadingIcon').hide();
                    alert("فشل الإتصال بالخادم , حاول مرة أخرى");
                }

            }  else if(data.hasOwnProperty('hamlaAdvantages')){

                var list = data.hamlaAdvantages;
                $('.loadingIcon').hide();

                for (var i = 0; i < list.length; i++) {


                    var temp = {
                        id: list[i].advantageId,
                        text: list[i].advantageText,
                        edit: false
                    }
                    var item =
                        '<div class="singleAdvantage">'+
                        '<div class="advantageIndex"><span>'+(i+1)+'</span></div>'+
                        '<div class="advantageDetails">'+temp.text+'</div>'+
                        '</div>'


                    $('#momayazatWrapper .advantages').append(item);

                }

                if(list.length == 0){
                    var item =
                        '<div class="singleAdvantage">'+
                        '<div class="advantageIndex"><span>'+(0)+'</span></div>'+
                        '<div class="advantageDetails">'+'لا توجد مميزات مضافة حتي الآن'+'</div>'+
                        '</div>'


                    $('#momayazatWrapper .advantages').append(item);
                }


            }

        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            $('.loadingIcon').hide();
            alert("فشل الإتصال بالخادم , حاول مرة أخرى");
        }

    });


}

function competitions(myDay){



    $('header h3').html(hamalaDays[myDay-1]);
    var task_timestamp = localStorage.getItem('task_timestamp');
    var notification_tstamp = localStorage.getItem('Time_stamp');

    if($('#slideDay2'+(myDay-1)).html() == '') {
        $('.loadingIcon').show();

        var info = {
            hamlaId: hamla_id,
            dayId: myDay.toString()

        }

        $.ajax({
            url: 'http://api.mutawef.com/HamlaCompetition/Index',
            dataType: 'json',
            type: 'POST',
            data: JSON.stringify(info),
            success: function (data) {


                if (data.hasOwnProperty('result')) {
                    if (data.result.hasOwnProperty('serverError')) {
                        $('.loadingIcon').hide();
                        alert("فشل الإتصال بالخادم , حاول مرة أخرى");
                    }

                } else if (data.hasOwnProperty('hamlaCompetitions')) {

                    var list = data.hamlaCompetitions;
                    $('.loadingIcon').hide();


                    var activityLen = list.length;
                    var count = 0;

                    var locations = new Array();
                    if (activityLen == 0) {
                        AllLocations[(myDay - 1)] = locations;
                        var item = '<div class="tableActivity">' +
                            '<div class="activityIndex"><span>' + (0) + '</span></div>' +
                            '<div class="activityDetails">' + 'لا توجد اســـئلة اليوم' + '</div>' +
                            '</div>';


                        $('#slideDay2' + (myDay - 1)).html('');
                        $('#slideDay2' + (myDay - 1)).append(item);
                    } else {


                        for (var i = 0; i < list.length; i++) {


                            var temp = {
                                id: list[i].CompetitionId,
                                text: list[i].CompetitionsQuestion,
                                edit: false
                            }
                            var item = ' <div class="tableActivity">' +
                                '<div class="activityIndex"><span>' + (i + 1) + '</span></div>' +
                                '<div class="activityDetails">' + temp.text + '</div>' +
                                '</div>';

                            $('#slideDay2' + (myDay - 1)).append(item);
                            tasks.push(temp.id);

                        }
                    }

                }

            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                $('.loadingIcon').hide();
                alert("فشل الإتصال بالخادم , حاول مرة أخرى");
            }

        });
    }
}
function phone() {


    $('.loadingIcon').show();

    var info ={
        hamlaId :hamla_id

    }

    $.ajax({
        url:'http://api.mutawef.com/HamlaDalel/Index',
        dataType:'json',
        type:'POST',
        data: JSON.stringify(info),
        success: function (data) {


            if (data.hasOwnProperty('result')) {
                if(data.result.hasOwnProperty('serverError'))
                {
                    $('.loadingIcon').hide();
                    alert("فشل الإتصال بالخادم , حاول مرة أخرى");
                }

            }  else if(data.hasOwnProperty('guides')){

                var list = data.guides;
                $('.loadingIcon').hide();
                $('#daleelWrapper').html('');
                for (var i = 0; i < list.length; i++) {


                    var temp = {
                        id: list[i].guidId,
                        name: list[i].giudeName,
                        phone: list[i].guidPhone
                    }
                    phones.push(temp);
                    var item =
                        '<div class="large_guide clearfix">'+
                        '<div>'+
                        '<p>'+temp.name+'</p>'+
                        '<p>'+temp.phone+'</p>'+
                        '</div>'+
                        '<div class="largeDivFooter">'+
                        '<a><img src="img/sms.png"  class="sms"/></a>'+
                        '<a><img src="img/phone.png"  class="phone"/></a>'+
                        '</div>'+
                        '</div>'+
                        '<div class="clear"></div>';


                    $('#daleelWrapper').append(item);

                }
                $('#daleelWrapper').append('<br><br><br>');

                $('.large_guide img').click(function () {

                    var i = $(this).parents('.large_guide').index(".large_guide");

                    var elem_class = $(this).attr('class');
                    var elem_id = $(this).attr('id');
                    var number = null;
                    number = phones[i].phone ;


                    if (number != null) {
                        if (elem_class == 'sms') {
                            //window.open('hejj.html','_blank');

                            window.location = 'sms:' + number;

                        }
                        else {

                            window.location = 'tel:' + number;

                        }
                    }
                });


            }

        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            $('.loadingIcon').hide();
            alert("فشل الإتصال بالخادم , حاول مرة أخرى");
        }

    });



}




function initialize() {


    $('.map_container').show();

    var str;
    var map;
    var infowindow;
    var i;
    //var lat= localStorage.getItem("lat");
    // var log= localStorage.getItem("long");
    // alert(currentDay)
    var locations = new Array();
    locations = AllLocations[currentDay];


    if (locations.length > 0) {
        var mapOptions = {

            center: new google.maps.LatLng(locations[0].lat, locations[0].lon),
            zoom: 12,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        map = new google.maps.Map(document.getElementById("map_canvas"),
            mapOptions);
    }
    else{
        var mapOptions = {

            center: new google.maps.LatLng(21.42232997953194, 39.89511251449585),
            zoom: 12,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        map = new google.maps.Map(document.getElementById("map_canvas"),
            mapOptions);
    }
    for (i = 0; i < locations.length; i++) {


        // Creating a marker and positioning it on the map
        marker = new google.maps.Marker({
            position: new google.maps.LatLng(locations[i].lat, locations[i].lon),
            map: map

        });


        str = '<div id="content" class="msg" style="font-size:16px" >' +
        '<p style="margin:2px;" >' + locations[i].posName + '</p>' +

        '<div style="background-color:#869258; padding:2px; font-size:16px">' +
        '<p style="margin:2px;" >إضغط  لتصل</p>' +
        '</div>' +

        '</div>';
        infowindow = new google.maps.InfoWindow();
        infowindow.setContent(str);
        infowindow.open(map, marker);

        google.maps.event.addListener(marker, 'click', (function (marker, i) {
            return function () {

                var lat = marker.position.lat();
                var lng = marker.position.lng();
                //this.setMap(null);

                getGPS(lat, lng, map, 2);
            }
        })(marker, i));
    }//for end
}

<!--notifications-->
function notifications() {


    var count  = 1;
    $('.loadingIcon').show();
    $('#notificationWrapper .newsItems').html('');
    var info ={
        hamlaId :hamla_id

    }

    $.ajax({
        url:'http://api.mutawef.com/HamlaNews/Index',
        dataType:'json',
        type:'POST',
        data: JSON.stringify(info),
        success: function (data) {


            if (data.hasOwnProperty('result')) {
                if(data.result.hasOwnProperty('serverError'))
                {
                    $('.loadingIcon').hide();
                    alert("فشل الإتصال بالخادم , حاول مرة أخرى");
                }

            }  else if(data.hasOwnProperty('hamlaNews')){

                var list = data.hamlaNews;
                $('.loadingIcon').hide();
                count  = list.length;
                for (var i = 0; i < list.length; i++) {


                    var temp = {
                        id: list[i].newsId,
                        text: list[i].newsText,
                        edit: false
                    }
                    // our URL RegRx
                    var urlRegex = /\(?(?:(http|https|ftp):\/\/)?(?:((?:[^\W\s]|\.|-|[:]{1})+)@{1})?((?:www.)?(?:[^\W\s]|\.|-)+[\.][^\W\s]{2,4}|localhost(?=\/)|\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})(?::(\d*))?([\/]?[^\s\?]*[\/]{1})*(?:\/?([^\s\n\?\[\]\{\}\#]*(?:(?=\.)){1}|[^\s\n\?\[\]\{\}\.\#]*)?([\.]{1}[^\s\?\#]*)?)?(?:\?{1}([^\s\n\#\[\]]*))?([\#][^\s\n]*)?\)?/gi;

// some text with link


// magic
                    temp.text = temp.text.replace(urlRegex, function(url) {


                        if(url.indexOf('<br>') > 0) {
                            url = url.substring(0, url.indexOf('<br>'))
                        }
                        return ('<a href="'+url+'" target="_blank">'+url+'</a>');

                    })
                    var item =
                        '<div class="singleNewsItem">'+
                        '<div class="newsIndex"><span>'+(count)+'</span></div>'+
                        '<div class="newsDetails">'+temp.text+'</div>'+
                        '</div>'

                    $('#notificationWrapper .newsItems').prepend(item);
                    count--;

                }

                if(list.length == 0){
                    var item =
                        '<div class="singleNewsItem">'+
                        '<div class="newsIndex"><span>'+(0)+'</span></div>'+
                        '<div class="newsDetails">'+'لا يوجد أخــبــــار'+'</div>'+
                        '</div>'

                    $('#notificationWrapper .newsItems').prepend(item);
                }


            }

        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            $('.loadingIcon').hide();
            alert("فشل الإتصال بالخادم , حاول مرة أخرى");
        }

    });

}


function notification_details(ID) {
    $('.notification_container').show();

    $.ajax({

        url: 'http://204.187.101.75/mutawef2/API/notification_details.aspx?notification_id=' + ID,
        dataType: 'xml',
        timeout: 60000,
        type: 'GET',
        success: function (data) {
            $(data).find("notification").each(function () {
                var apprev = $(this).find("apprev").text();
                var details = $(this).find("details").text();

                $('.notification_detail center h3').html(apprev)
                $('.notification_detail p').html(details)
            });

        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {

            alert("فشل الإتصال بالخادم , حاول مرة أخرى");
            // loadslidshow();
        }
    });
}

function guideMe() {
    getGPS(21.416457141620075, 39.88618612289429, null, 1);
}

function hideDashboard(dashboardItemId) {

    $('.dashboardWrapper').fadeOut();
    $('#' + dashboardItemId + 'Wrapper').addClass('activeWrapper');
    $('#' + dashboardItemId + 'Wrapper').fadeIn();
    //  sliderOptions();
    $('.logOut').hide();
    $('.backIcon').show();
}
function showDashboard() {
    $('.loadingIcon').hide();

    var dashboardItemId = $('.activeWrapper').attr('id');

    $('#' + dashboardItemId).removeClass('activeWrapper');
    $('#' + dashboardItemId).fadeOut();
    $('.dashboardWrapper').fadeIn();

    if(dashboardItemId == 'tableWrapper'){
        $('.allFavIcon').hide();
    }
    $('header h3').html(hamla_name);
    $('.backIcon').hide();
    $('.logOut').show();
}
function logOut(){

    localStorage.setItem('first_use', '');
    window.location.href = 'check_user.html?device_id='+device_id+'&device_type='+0+'&current_day='+realDay;

}
function competitionsSlider(){

    $('.competitionsWrapper').slick({
        dots: true,
        infinite: false,
        rtl: true,
        speed: 300,
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: false,
        arrows: true,
        draggable: true,
        prevArrow:"<img class='rightArr' src='img/rightArrow.png'>",
        nextArrow:"<img class='leftArr' src='img/leftArrow.png'>"


    });

    if(sliderOpen2 == 0){
        var currentSlideGlobal = 0;
        var nextSlideGlobal = 0;
        $('.competitionsWrapper').on('beforeChange', function (event, slick, currentSlide, nextSlide) {

            currentSlideGlobal = currentSlide;
            nextSlideGlobal = nextSlide;


        });
        $('.competitionsWrapper').on('afterChange', function (event, slick, currentSlide) {

            localStorage.setItem('task_timestamp',0);
            //                   alert(currentSlideGlobal)
            //                    alert(nextSlideGlobal)
            currentDay = nextSlideGlobal;
            day = nextSlideGlobal+1;
//
            if(day == 1){
                $('.rightArr').hide();
                $('.leftArr').show();
            }else if(day == 8){
                $('.leftArr').hide();
                $('.rightArr').show();
            }else{
                $('.leftArr').show();
                $('.rightArr').show();
            }
            competitions((nextSlideGlobal+1))
        });
    }
    sliderOpen2 = 1;
    $('.competitionsWrapper').slick('slickGoTo', parseInt(day-1))


}
function sliderOptions(){


    $('.tableWrapper').slick({
        dots: true,
        infinite: false,
        rtl: true,
        speed: 300,
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: false,
        arrows: true,
        draggable: true,
        prevArrow:"<img class='rightArr' src='img/rightArrow.png'>",
        nextArrow:"<img class='leftArr' src='img/leftArrow.png'>"

//                    responsive: [
//                        {
//                            breakpoint: 1200,
//                            settings: {
//                                slidesToShow: 3,
//                                slidesToScroll: 3,
//                                infinite: true,
//                                dots: true
//                            }
//                        },
//                        {
//                            breakpoint: 991,
//                            settings: {
//                                slidesToShow: 2,
//                                slidesToScroll: 2
//                            }
//                        },
//                        {
//                            breakpoint: 600,
//                            settings: {
//                                slidesToShow: 1,
//                                slidesToScroll: 1
//                            }
//                        }
//                    ]
    });



    if(sliderOpen == 0){
        var currentSlideGlobal = 0;
        var nextSlideGlobal = 0;
        $('.tableWrapper').on('beforeChange', function (event, slick, currentSlide, nextSlide) {

            currentSlideGlobal = currentSlide;
            nextSlideGlobal = nextSlide;


        });
        $('.tableWrapper').on('afterChange', function (event, slick, currentSlide) {

            localStorage.setItem('task_timestamp',0);
            //                   alert(currentSlideGlobal)
//                    alert(nextSlideGlobal)
            currentDay = nextSlideGlobal;
            day = nextSlideGlobal+1;

            if(day == 1){
                $('.rightArr').hide();
                $('.leftArr').show();
            }else if(day == 8){
                $('.leftArr').hide();
                $('.rightArr').show();
            }else{
                $('.leftArr').show();
                $('.rightArr').show();
            }
            schedular((nextSlideGlobal+1))
        });

    }
    sliderOpen = 1;
    $('.tableWrapper').slick('slickGoTo', parseInt(day-1))
}

