'use strict';

/**
 * @ngdoc function
 * @name hamalatAdminApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the hamalatAdminApp
 */



hamalatAdminApp
  .controller('editProfile', ['$scope', '$rootScope', '$http', '$routeParams','$location','User','UserService',
    function ($scope,$rootScope, $http,  $routeParams,$location, User, UserService) {

      $rootScope.topTitle = 'حملتي';
      $rootScope.logOut= false;
      $rootScope.loading= false;
      $rootScope.backBtn= true;
      $scope.user= User.getUser();


      $scope.editedFields={
        campaignName:$scope.user.hamlaName,
        campaignGuide:$scope.user.name,
        campaignGuidePhone:$scope.user.phone
      };
      $rootScope.mainTitle= 'تعديل البيانات';

      $rootScope.backAction = function () {
        window.location.href ='#/profile';
      }

      $scope.editUserProfile = function (isValid) {


        var info ={
          userName:$scope.editedFields.campaignGuide,
          hamlaName:$scope.editedFields.campaignName,
          phone:$scope.editedFields.campaignGuidePhone,
          userId:$scope.user.id
        }


        if(isValid){
          $rootScope.loading= true;
          var res = UserService.updateUser(info);
          res.success(function (data) {
            $rootScope.loading= false;

              if(data.hasOwnProperty('serverError'))
              {
                $rootScope.message= 'حدث خطأ اثناء الاتصال بالخادم .. من فضلك حاول مرة أخري';
                $('#failPopup').modal('show');
              }
              else if (data.hasOwnProperty('result')) {

                if (data.result == true) {


                  User.setUser($scope.user.id, info.userName, $scope.user.email,info.phone ,$scope.user.hamlaId, info.hamlaName, $scope.user.hamlaCode, 'loggedIn');
                  $rootScope.user = User.getUser();

                  $rootScope.message = 'تم حفظ التعديلات بنجاح';
                  //$('#successPopup').modal('show');
                  $('#successOkOnly').modal({
                    backdrop: 'static',
                    keyboard: false
                  }).modal('show');
                } else {
                  $rootScope.message = 'حدث خطأ اثناء الاتصال بالخادم .. من فضلك حاول مرة أخري';
                  $('#failPopup').modal('show');
                }
              }


          });
          res.error(function (err) {
            $rootScope.loading= false;
            $rootScope.message = 'حدث خطأ اثناء الاتصال بالخادم .. من فضلك حاول مرة أخري';
            $('#failPopup').modal('show');
          });
        }
      }
      $rootScope.okOnPopup = function () {
        window.location.href = "#/profile";
      }

      $scope.goToProfile = function () {
        window.location.href = '#/profile'

      }

    }]);
