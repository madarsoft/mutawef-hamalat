hamalatAdminApp
  .factory('UserService',['$http','$rootScope', function($http,$rootScope) {

    return{
      login : function(user) {
        return $http.post($rootScope.domain+'HamlaUser/Login', user);
      } ,
      register : function(info) {
        return $http.post($rootScope.domain+'Hamlauser/Register', info);

      },
      validateEmail : function(email){
        return $http.post($rootScope.domain+'HamlaUser/CheckEmail', email);
      },
      resetMyPassword : function(password) {
        return $http.post($rootScope.domain+'HamlaUser/ResetPassword', password);
      },
      forgotMyPassword : function(email) {
        return $http.post($rootScope.domain+'HamlaUser/ForgetPassword', email);
      },
      updateUser: function(user){
        return $http.post($rootScope.domain+'HamlaUser/edit', user);
      },
      getProfileInfo : function(userID) {
        return $http.post($rootScope.domain+'HamlaUser/index', userID);
      }
    };

  }]);
