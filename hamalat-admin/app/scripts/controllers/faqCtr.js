'use strict';

/**
 * @ngdoc function
 * @name hamalatAdminApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the hamalatAdminApp
 */
angular.module('hamalatAdminApp')
  .controller('faqCtr', ['$scope', '$rootScope', '$http', '$routeParams','$location',
    function ($scope,$rootScope, $http,  $routeParams,$location ) {

      $rootScope.logOut= false;
      $scope.page = 1;
      $scope.numberPerPage = 5;
      $scope.langObject = null;
      $scope.quId='';

      $scope.loading = true;
     $rootScope.mainTitle = 'الأسئــلة الشــائعة';
      $rootScope.topTitle = '';

      function getCurrentItemId(){

        var url = window.location.href;
        var index = url.lastIndexOf('#');

        $scope.quId = '';
        if(index != 1)
        {
          $scope.quId = url.substring(index+1, url.length);

        }

      }


      $scope.LoadCategory = function(){
        var isEmpty = true;
        $scope.loading = true;
        $scope.faqQuestions = [];
        //http://api.madarsoft.com/HelpDesk/v1/category/index

        var authInfo = "iVlfZQkhEISYqSRJGn4";

        $.ajax({
          url:'https://madarsoft.freshdesk.com/support/solutions/folders/9000171799.json',
          dataType:'json',
          type:'GET',

          beforeSend: function(xhr) {
            xhr.setRequestHeader("Authorization", "Basic " + btoa(authInfo))
          },
          success:function(data) {

            $scope.loading = false;
            // $('#loadingDiv').hide();
            var obj;

            var list =data.folder.published_articles

            var len = (list || []).length

            for (var i = 0; i < len; i++) {

              obj = list[i];
              isEmpty = false;
              var obj2 = {
                messageHeader : list[i].title,
                messageBody : list[i].description,
                index: list[i].id
              }



              $scope.$apply(function () {
                $scope.faqQuestions.push(obj2)
              });
              // showData(obj2);

              if((i+1) == len){


               // $scope.openCurrentQu();


              }


            }// articles loop

            if(isEmpty){

              $rootScope.message = 'لا توجد أسئــلة حتي الآن'

              $('#failPopup').modal('show');
            }

          },
          error:function(XMLHttpRequest,textStatus, errorThrown)
          {

            $rootScope.loading= false;
            $rootScope.message= 'حدث خطأ اثناء الاتصال بالخادم .. من فضلك حاول مرة أخري';
            $('#failPopup').modal('show');

          }
        });
      }


      $scope.add_remove_hash = function(itemId){

        if($('#hash'+itemId).hasClass('collapsed')){
          $scope.addHash($('#hash'+itemId).find('.plus'));
        }else {
          $scope.removeHash();
        }


      }

      $scope.addHash = function(item){

        var id = $(item).parents('.panel').attr('id');

        $location.hash(id);


        // $location.url($location.path()+"#"+id).replace().reload(false)
      }
      //$scope.$on('$locationChangeStart', function(ev) {
      //  ev.preventDefault();
      //});
      $scope.removeHash = function(){
        var url = window.location.href;
        var index = url.lastIndexOf('#');


        if(index != 1)
        {
          $scope.quId = '';
          $location.hash('');

        }
      }


      $scope.openCurrentQu = function(){


        if($scope.quId != '') {

          $location.hash($scope.quId);
          $anchorScroll.yOffset = 100;   // always scroll by 50 extra pixels
          $anchorScroll();
          $('#collapse' + $scope.quId).addClass("in");
          $('#href' + $scope.quId).removeClass('collapsed');
          $('#href' + $scope.quId).attr('aria-expanded', true);
          // $('html,body').animate({ scrollTop: $('#'+$scope.quId).offset().top -($('.panel-title').height()*2)}, 200);

          // return false;

          // e.preventDefault();
        }

      }

      getCurrentItemId();
      $scope.LoadCategory();



    }]);
