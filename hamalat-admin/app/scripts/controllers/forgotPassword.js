'use strict';

/**
 * @ngdoc function
 * @name hamalatAdminApp.controller:LoginCtrl
 * @description
 * # LoginCtrl
 * Controller of the hamalatAdminApp
 */
hamalatAdminApp
  .controller('forgotPassword', ['$scope', '$rootScope', '$http', '$routeParams','$location','User','UserService',
    function ($scope,$rootScope, $http,  $routeParams,$location, User, UserService) {

      $rootScope.topTitle = 'حملتي';
      $rootScope.loading= false;
      $rootScope.backBtn= true;
      $scope.userEmail = '';
      $rootScope.logOut= false;
      $rootScope.mainTitle = 'نسيت كلمة المرور' ;

      $rootScope.backAction = function () {
        window.location.href ='#/login';
      }

     $scope.sendEmail = function (isValid) {

       if(isValid) {

         var userEmail = {
           email:$scope.userEmail
         };


         var res = UserService.forgotMyPassword(userEmail);

         res.success(function (data) {
           if (data.hasOwnProperty('serverError')) {
             $rootScope.message = 'حدث خطأ اثناء الاتصال بالسيرفر .. من فضلك حاول مرة أخري';
             $('#failPopup').modal('show');
           }
           else if (data.hasOwnProperty('result')) {
             if(data.result == false){
               $rootScope.message = 'البريد الإلكتروني الذي ادخلته غير مسجل لدينا';
               $('#failPopup').modal('show');
             }
             else{
               $rootScope.message = 'تم إرسال بريد الكتروني لك ... لاستكمال عملية استعادة كلمة المرور توجه إلي بريدك الألكتروني';
               $('#successOkOnly').modal({
                 backdrop: 'static',
                 keyboard: false
               }).modal('show');
             }
           }
         });
         res.error(function (err) {
           $rootScope.message = 'حدث خطأ اثناء الاتصال بالسيرفر .. من فضلك حاول مرة أخري';
           $('#failPopup').modal('show');
         });
       }

      };

      //$scope.returnToHome = function () {
      //  window.location.href = "#/login";
      //
      //};

      $rootScope.okOnPopup = function () {
        window.location.href = "#/login";
      }

    }]);
