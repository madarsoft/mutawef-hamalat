'use strict';

/**
 * @ngdoc function
 * @name hamalatAdminApp.controller:LoginCtrl
 * @description
 * # LoginCtrl
 * Controller of the hamalatAdminApp
 */
hamalatAdminApp
  .controller('scheduleCtrl', ['$scope', '$rootScope', '$http', '$routeParams','$location','ScheduleService','timeService',
    function ($scope,$rootScope, $http,  $routeParams,$location, ScheduleService, timeService) {


      $rootScope.loading= false;
      $rootScope.backBtn= true;
      $rootScope.logOut= false;
      $rootScope.message = '';
      $rootScope.topTitle = 'حملتي';


      $rootScope.loading= true;
      var user = $rootScope.user ;
      $scope.myDayIndex = 0;
      var sliderOpen= 0;

      if(user != ''){
        $scope.hamlaId = user.hamlaId;

      }

      $scope.days =[
        {id:'1', name:'ما قبل التروية', loaded :false},
        {id:'2', name:'الثامن من ذى الحجة(التروية)', loaded :false},
        {id:'3', name:'التاسع من ذي الحجة (يوم عرفة)', loaded :false},
        {id:'4', name:'العاشر من ذي الحجة (يوم النحر)', loaded :false},
        {id:'5', name:'أول أيام التشريق', loaded :false},
        {id:'6', name:'ثانى أيام التشريق', loaded :false},
        {id:'7', name:'ثالث أيام التشريق', loaded :false},
        {id:'8', name:'اليوم الأخير', loaded :false}
      ];

      $scope.scheduleTable = [{day:[]}, {day:[]},  {day:[]} , {day:[]} ,{day:[]}, {day:[]},{day:[]} ,{day:[]}];

      $scope.daySchedule = [];
      $scope.editeText ='';
      $scope.inputParagraph = {
        text:''
      };

      $scope.removeIndex = 0;
    ///////////////////////////////////////////////////////////////

      $rootScope.backAction = function () {
        window.location.href ='#/dashboard';
      }

      var id= 0;
      $scope.loadDay = function() {


        $rootScope.mainTitle =  $scope.days[$scope.myDayIndex].name;
        $('header h3').html($rootScope.mainTitle )


        for (var i = id; i < (($scope.myDayIndex+1) *10); i++) {
          var p = {
            id: i + 1,
            text: 'paragraph' + (i + 1),
            edit: false
          }

          $scope.$apply(function(){
            $scope.days[$scope.myDayIndex].loaded = true;
            $scope.scheduleTable[$scope.myDayIndex].day.push(p);
          })


        }

        id= ($scope.myDayIndex+1)  * 10;

        console.log($scope.scheduleTable)

      }

      /////////////////////////////////////////////////////////////



      $scope.getDailySchedule = function () {

        $scope.$apply(function(){
          $rootScope.mainTitle =  $scope.days[$scope.myDayIndex].name;
        })


        if( $scope.days[$scope.myDayIndex].loaded == false){

          var info ={
            hamlaId :$scope.hamlaId,
            dayId :$scope.days[$scope.myDayIndex].id

          }
        //  $rootScope.loading= true;
          var res =  ScheduleService.viewSchedule(info)
          res.success(function (data) {

            $rootScope.loading= false;
            if (data.hasOwnProperty('result')) {
              if(data.result.hasOwnProperty('serverError'))
              {
                $rootScope.message= 'حدث خطأ اثناء الاتصال بالخادم .. من فضلك حاول مرة أخري';
                $('#failPopup').modal('show');
              }

            }

            else if(data.hasOwnProperty('hamlaPrograms')){

              var list = data.hamlaPrograms;
              for(var i = 0; i < list.length; i ++){
                var item = list[i];
                if(item.lat == null || item.lng == null)
                {
                  item.lat = '';
                  item.lng='';
                }
                if(item.imageUrl.lastIndexOf('/')+1 == item.imageUrl.length){
                  item.imageUrl = '';
                }
                var convertedTime = timeService.convertTo12Hour(item.time.Hours+':'+item.time.Minutes);
                var temp = {

                  id:item.programId,
                  day: item.dayId,
                  title :item.title,
                  details: item.programInfo.replace(/\r?\n/g,'<br/>'),
                  image:item.imageUrl,
                  lat:item.lat,
                  long:item.lng,
                  phone:item.responserPhone,
                  name :item.responserName,
                  time:convertedTime.split(' ')[0],
                  am_pm: timeService.arabicMode(convertedTime.split(' ')[1])

                }
                $scope.scheduleTable[$scope.myDayIndex].day.push(temp);
                $scope.days[$scope.myDayIndex].loaded = true;


              }
              console.log($scope.scheduleTable[$scope.myDayIndex].day);

            }
          });
          res.error(function (err) {

            $rootScope.loading= false;
            $rootScope.message= 'حدث خطأ اثناء الاتصال بالخادم .. من فضلك حاول مرة أخري';
            $('#failPopup').modal('show');
          });
        }




      }



      $scope.loadMap = function (index){

        var program = $scope.scheduleTable[$scope.myDayIndex].day[index]
        if(program.lat == '' || program.long == ''){
          $rootScope.loading= false;
          $rootScope.message= 'لم يتم تحديد موقعه علي الخريطة';
          $('#failPopup').modal('show');
        }else{
          $scope.map(program.lat,program.long,'موقع البرنامج');
        }

      }
      $scope.map = function(lat ,long, mapTitle) {

        $('#mapPopUp').modal('show');
        $('#mapTitle').html(mapTitle);
        var mapOptions = {
          zoom: 15,
          center: new google.maps.LatLng(lat, long)
        };
        var map = new google.maps.Map(document.getElementById('map'),
          mapOptions);


        var myLatlng = new google.maps.LatLng(lat, long);
        var marker = new google.maps.Marker({
          position: myLatlng,
          map: map

        });
        var infowindow =new google.maps.InfoWindow();
        infowindow.setContent(mapTitle);
        infowindow.open(map, marker);



        setTimeout(function(){

          google.maps.event.trigger(map, 'resize');
          map.setZoom( map.getZoom() );
        }, 1000)



      }

      $scope.addNewOne = function (){

       window.location.href = '#/addSchedule?programId=0';

      }




      var day ;
      var sliderOpen = 0
      setTimeout(function () {initSlick()}, 100);
      function initSlick(){

        if(sliderOpen == 0) {


          $('.schedule').slick({
            rtl: true,
            infinite: true,
            speed: 600,
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: true,
            //dots: true,
            prevArrow:"<a class='rightArr'><i class='ion-ios-arrow-right'></i></a>",
            nextArrow:"<a class='leftArr'><i class='ion-ios-arrow-left'></i></a>"
          });


          var currentSlideGlobal = 0;
          var nextSlideGlobal = 0;
          $('.schedule').on('beforeChange', function (event, slick, currentSlide, nextSlide) {

            currentSlideGlobal = currentSlide;
            nextSlideGlobal = nextSlide;


          });
          $('.schedule').on('afterChange', function (event, slick, currentSlide) {

            //alert('eeee')
            var currentDay = nextSlideGlobal;
            var day = nextSlideGlobal + 1;

            console.log('afterChange' + day);

            console.log('now' + currentSlideGlobal);
            console.log('next' + nextSlideGlobal);
            $scope.myDayIndex = currentDay;

            if($scope.myDayIndex == 0){
              $('.rightArr').hide();
              $('.leftArr').show();
            }else if($scope.myDayIndex == 7){
              $('.leftArr').hide();
              $('.rightArr').show();
            }else{
              $('.leftArr').show();
              $('.rightArr').show();
            }

            if(currentSlideGlobal != nextSlideGlobal) {
              $('html,body').animate({scrollTop: $('.schedule').offset().top - ($('.schedule').height() * 2)}, 200);
            }
            $scope.getDailySchedule();

          });

        }

        sliderOpen = 1;
        $('.schedule').slick('slickGoTo', parseInt($rootScope.realDay -1 ))
      }
      $rootScope.cancelDialogPopup = function () {

        $('#dialogPopup').modal('hide');
        $scope.removeIndex = 0;
      }

      $rootScope.okDialogPopup = function () {

        $('#dialogPopup').modal('hide');
        $scope.removeParagraph();
      }


    }]);
