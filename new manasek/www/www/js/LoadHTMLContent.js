function loadJsonFile (filename){
	
      $.ajax({
        url: 'json/ar/'+filename+'.json',
        dataType: 'json',
        timeout: 60000,
        type: 'GET',
        cache:'false',
        success: function (data) {
			for(var i = 0; i < data.mansak.tabsHTML.length ; i++){
				
				$('.tab-links').append(data.mansak.tabsHTML[i].tab)
				
			}
			
			for(var j = 0; j < data.mansak.tabsContent[0].tabDetails.length ; j++){
				
				var step = data.mansak.tabsContent[0].tabDetails[j].step;
				$('#tab1').append(step)
				
			}
			
			for(var j = 0; j < data.mansak.tabsContent[1].tabDetails.length ; j++){
				
				var step = data.mansak.tabsContent[1].tabDetails[j].step;
				$('#tab2').append(step)
				
			}
			jQuery('.tabs .tab-links a').on('click', function(e)  {
        		jQuery(this).parent('li').addClass('active').siblings().removeClass('active');
        		var currentAttrValue = jQuery(this).attr('href'); 
       
        		// Show/Hide Tabs
				jQuery('.tabs ' + currentAttrValue).siblings().hide();
				jQuery('.tabs ' + currentAttrValue).show();
 
        		// Change/remove current tab to active
         		e.preventDefault();
    		});

          

        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
           // Message('#note_popup',"Error In getCachedCities : " + err.message);

        }
    });
	
	
}


