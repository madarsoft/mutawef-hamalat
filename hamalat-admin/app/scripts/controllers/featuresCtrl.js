'use strict';

/**
 * @ngdoc function
 * @name hamalatAdminApp.controller:LoginCtrl
 * @description
 * # LoginCtrl
 * Controller of the hamalatAdminApp
 */
hamalatAdminApp
  .controller('featuresCtrl', ['$scope', '$rootScope', '$http', '$routeParams','$location','FeaturesService',
    function ($scope,$rootScope, $http,  $routeParams,$location, FeaturesService) {

      $rootScope.topTitle = 'حملتي';
      $rootScope.backBtn= true;
      $rootScope.message = '';
      $rootScope.loading= false;
      $rootScope.logOut= false;
      $rootScope.mainTitle= 'مميزات الحملة';
      var user = $rootScope.user ;



      if(user != ''){
        $scope.hamlaId = user.hamlaId;

      }




      $scope.featuresList = [];
      $scope.editeText ='';
      $scope.inputParagraph = {
        text:''
      };
      $scope.removeIndex = 0;

      //////////////////////////////////////////////////////////////////


      $rootScope.backAction = function () {
        window.location.href ='#/dashboard';
      }


      $scope.loadDay = function() {

        $scope.featuresList=[]
        // alert($scope.myDayIndex)
        for (var i = 0; i <  10; i++) {
          var p = {
            id: i + 1,
            text: 'paragraph' + (i + 1),
            edit: false
          }


          $scope.featuresList.push(p);



        }





      }
      //$scope.loadDay();
      /////////////////////////////////////////////////////////////



      $scope.getFeatures = function () {
        $scope.featuresList = [];

        var info ={
          hamlaId :$scope.hamlaId

        }
        $rootScope.loading= true;
        var res =  FeaturesService.viewFeatures(info)
        res.success(function (data) {

          $rootScope.loading= false;

          if(data.hasOwnProperty('hamlaAdvantages')){

            var list = data.hamlaAdvantages;
            for(var i = 0; i < list.length; i ++){
              var temp = {
                id:list[i].advantageId,
                text:list[i].advantageText,
                edit:false
              }

              $scope.featuresList.push(temp);
            }
          }
          else {

            $rootScope.message= 'حدث خطأ اثناء الاتصال بالخادم .. من فضلك حاول مرة أخري';
            $('#failPopup').modal('show');

          }

        });
        res.error(function (err) {
          $rootScope.loading= false;
          $rootScope.message = 'حدث خطأ اثناء الاتصال بالسيرفر .. من فضلك حاول مرة أخري';

          $('#failPopup').modal('show');
        });





      }

      $scope.changeToEditMode= function (index){

        $scope.featuresList[index].edit= true;
        $scope.editeText =  $scope.featuresList[index].text;
        $scope.featuresList[index].text = $scope.featuresList[index].text.replace(/<br\s*\/?>/mg,"\n");
        $('.stepInput textarea').height('95%')
        $('.stepInput textarea').css('min-height', '150px');
        $scope.closeOtherEdits(index);

      }

      $scope.closeOtherEdits = function(index){
        for(var i =0; i < $scope.featuresList.length ; i ++){
          if(i != index) {
            $scope.featuresList[i].edit = false;
          }
        }
      }


      $scope.cancelEdit= function (index){


        $scope.featuresList[index].text = $scope.editeText;

        $scope.featuresList[index].edit= false;
      }




      $scope.addParagraph= function (index){

        var temp = {
          hamlaId:$scope.hamlaId,
          text:$scope.inputParagraph.text.replace(/\r?\n/g,'<br/>'),
          title:''
        }

        $rootScope.loading= true;
        var res =  FeaturesService.addParagraph(temp)
        res.success(function (data) {
          $rootScope.loading= false;



          if(data.hasOwnProperty('advantageId')){



            var temp = {
              id:data.advantageId,
              text:$scope.inputParagraph.text.replace(/\r?\n/g,'<br/>'),
              edit:false
            }

            $scope.featuresList.push(temp);
            $scope.inputParagraph.text = '';
            $rootScope.message= 'تم اضافة ميــزة الحملة بنجاح';
            $('#successPopup').modal('show');

          }else{
            $rootScope.message= 'حدث خطأ اثناء الاتصال بالخادم .. من فضلك حاول مرة أخري';
            $('#failPopup').modal('show');
          }



        });
        res.error(function (err) {
          $rootScope.loading= false;
          $rootScope.message = 'حدث خطأ اثناء الاتصال بالسيرفر .. من فضلك حاول مرة أخري';

          $('#failPopup').modal('show');
        });

      }


      $scope.saveEdit= function (index){
        var currentParagraph =  $scope.featuresList[index];
        var info ={
          advantageId :(currentParagraph.id).toString(),
          text :currentParagraph.text.replace(/\r?\n/g,'<br/>'),
          title:''

        }


        $rootScope.loading= true;
        var res =  FeaturesService.updateParagraph(info)
        res.success(function (data) {
          $rootScope.loading= false;
          if (data.hasOwnProperty('result')) {
            if(data.result.hasOwnProperty('serverError'))
            {
              $rootScope.message= 'حدث خطأ اثناء الاتصال بالخادم .. من فضلك حاول مرة أخري';
              $('#failPopup').modal('show');
            }
            else if(data.result == true){
              $scope.featuresList[index].text = $scope.featuresList[index].text.replace(/\r?\n/g,'<br/>');
              $scope.featuresList[index].edit= false;
              $rootScope.message= 'تم حفظ التعديلات بنجاح';
              $('#successPopup').modal('show');
            }else{
              $rootScope.message= 'حدث خطأ اثناء الاتصال بالخادم .. من فضلك حاول مرة أخري';
              $('#failPopup').modal('show');
            }

          }

        });
        res.error(function (err) {
          $rootScope.loading= false;
          $rootScope.message = 'حدث خطأ اثناء الاتصال بالسيرفر .. من فضلك حاول مرة أخري';

          $('#failPopup').modal('show');
        });

      }

      $scope.confirmRemoveParagraph= function (index){
        $rootScope.removeIndex = index;
        $rootScope.message = 'هل تريد فعلا مســح الميـــزة';
        $('#dialogPopup').modal('show');

      }
      $scope.removeParagraph= function (){

        var index =$rootScope.removeIndex;

        var id= {
          advantageId: ($scope.featuresList[index].id).toString()
        }




        $rootScope.loading= true;
        var res =  FeaturesService.removeParagraph( id)
        res.success(function (data) {
          $rootScope.loading= false;


          if (data.hasOwnProperty('result')) {
            if(data.result.hasOwnProperty('serverError'))
            {
              $rootScope.message= 'حدث خطأ اثناء الاتصال بالخادم .. من فضلك حاول مرة أخري';
              $('#failPopup').modal('show');
            }
            else if(data.result == true){
              $scope.featuresList.splice(index, 1);

              $rootScope.message= 'تم مســح الميـــزة بنجاح';
              $('#successPopup').modal('show');

            }else{
              $rootScope.message= 'حدث خطأ اثناء الاتصال بالخادم .. من فضلك حاول مرة أخري';
              $('#failPopup').modal('show');
            }

          }

        });
        res.error(function (err) {
          $rootScope.loading= false;
          $rootScope.message = 'حدث خطأ اثناء الاتصال بالسيرفر .. من فضلك حاول مرة أخري';

          $('#failPopup').modal('show');
        });
      }

      $rootScope.cancelDialogPopup = function () {

        $('#dialogPopup').modal('hide');
        $scope.removeIndex = 0;
      }

      $rootScope.okDialogPopup = function () {

        $('#dialogPopup').modal('hide');
        $scope.removeParagraph();
      }

      $scope.getFeatures();




    }]);
