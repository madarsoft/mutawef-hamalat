


var newsId = '';
var newsDetails = '';
var isMobile = true;
var sloginList = [];
var shareUrl = '';
var shorlSharUrl ='';
var twitterTitle = '';
var iphoneDevice = false;
var androidDevice = false;
var app= false;


function getUrlVars() {

    var vars = {};
    var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
        vars[key] = value;
    });
    return vars;
}

function init() {


    newsId = decodeURI(getUrlVars()["id"]).split("+").join(" ");

    newsDetails = {
        newsId :newsId,
        title:'',
        abbrev:'',
        content:'',
        main_image:'',
        date:'',
        facebook_id:''

    }

    shareUrl = 'http://mutawef.com/share/share/index?id=' + newsDetails.newsId
    shorlSharUrl = shareUrl;






    if(navigator.userAgent.match(/Android/) || navigator.userAgent.match(/iPhone/) || navigator.userAgent.match(/iPad/) || navigator.userAgent.match(/iPod/))
    {

        isMobile = true;

    }

    else{

        isMobile = false;
    }
    if( !isMobile){

    }


    if ( navigator.userAgent.match(/iPhone/) || navigator.userAgent.match(/iPad/) || navigator.userAgent.match(/iPod/)) {
        iphoneDevice = true;
        iOSversion();

    }else if(navigator.userAgent.match(/Android/) ) {
        androidDevice = true;
        $('.iframeContainer').css('text-align', 'justify');
    }


    getApp();
    automaticDeepLinking();

}

function iOSversion() {
    var str = navigator.userAgent;
    var n =  str.indexOf("OS");
    n = n + 3;
    str= str.substring(n , str.length)
    str= str.substr(0,str.indexOf(' ')); // "72"
    str= str.substr(str.indexOf(' ')+1);
    str = str.split('_');

    if(parseInt(str[0]) !=10 ||  parseInt(str[1]) !=3 )
    {

        $('.iframeContainer').css('text-align', 'justify');

    }
}

function deepLinking(){
    if(iphoneDevice){
        window.location='https://itunes.apple.com/us/app/المطوف-مناسك-الحج-والعمرة-و-الزيارة-خطوة-بخطوة/id547963253?mt=8';

    }else {

        window.location='https://play.google.com/store/apps/details?id=com.madar';

    }
}



function getApp(){

    if(androidDevice){
        $('#downloadApp').attr('href','https://play.google.com/store/apps/details?id=com.madar');
        $('#openApp').attr('href','intent://app/SplashScreen?id='+newsDetails.newsId+'#Intent;package=com.queen.ui;scheme=queen;end;');
    }else if(iphoneDevice){
        $('#downloadApp').attr('https://itunes.apple.com/us/app/المطوف-مناسك-الحج-والعمرة-و-الزيارة-خطوة-بخطوة/id547963253?mt=8');
        $('#openApp').attr('href',"queen://queen/"+newsDetails.newsId);
    }
}


function automaticDeepLinking() {


    if (androidDevice && !app) {
        // alert('android')
        location.href = 'intent://app/SplashScreen?id='+newsDetails.newsId+'#Intent;package=com.queen.ui;scheme=queen;end;';

    }else if (iphoneDevice && !app ) {


        window.location = "queen://queen/"+newsDetails.newsId;

    }


}


function closeDownload(){
    $('#browserHeader').hide();
}


function facebookShare(){

    window.open("http://www.facebook.com/sharer.php?u="+encodeURIComponent(shareUrl),'_system', 'location=no');


}
function twitterShare() {

    var twitterTxt =newsDetails.title + '\r\n' +shorlSharUrl;
    twitterTxt=   encodeURIComponent(twitterTxt);
//http://stackoverflow.com/questions/23095906/how-to-open-twitter-and-facebook-app-with-phonegap
//http://wiki.akosma.com/IPhone_URL_Schemes#Twitter
    // If Mac//


    if (iphoneDevice && app) {
        setTimeout(function () {
                window.open('https://itunes.apple.com/ca/app/twitter/id333903271?mt=8', '_system', 'location=no'); }
            ,25);
        window.open('twitter://post?message=' + twitterTxt, '_system', 'location=no');

    }

//If Android


    else if (androidDevice && app) {


    }

    else {

        //window.open( 'https://twitter.com/intent/tweet?url='+twitterTxt+'&text='+$scope.twitterTitle , 'href','popup');
        window.open('https://twitter.com/share?url='+escape(shorlSharUrl)+'&text='+'\r\n' +encodeURIComponent(newsDetails.title)+'\r\n'  , '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,width=600');
        return false;
        return false;
    }
}




function whatsappShare(){
    if (androidDevice && app) {
        // window.location.href = 'whatsappShare://';
    }else{
        var whatsappTxt =newsDetails.title + '\r\n' + newsDetails.abbrev + '\r\n' + shorlSharUrl +'\r\n'+'تم نشر هذة المقالة من تطبيق المطوف';

        whatsappTxt=   encodeURIComponent(whatsappTxt);
        window.open('whatsapp://send?text='+ whatsappTxt , 'href', 'popup');
        return false;
    }

}
function mailShare(){
    if (androidDevice && app) {
        // window.location.href = 'mailShare://';
    }else {
        var subject = newsDetails.title;
        var emailBody ='<p>'+newsDetails.title +'</p>'+ '\r\n' + '<p>'+newsDetails.abbrev +'</p>'+'\r\n' +'\r\n' +shorlSharUrl + '\r\n' +'\r\n'+'<img src="'+newsDetails.main_image +'" width="100%">'+ '\r\n'+'تم نشر هذة المقالة من تطبيق المطوف';
        emailBody=   encodeURIComponent(emailBody);
        window.open('mailto: ?subject=' + subject + '&body=' + emailBody, 'href', 'popup');
        return false;
    }
}


function telegramShare(){
    if (androidDevice && app) {
        // window.location.href = 'telegramShare://';
    }else {
        var telegramTxt =newsDetails.title + '\r\n' + newsDetails.abbrev + '\r\n' + shorlSharUrl +'\r\n'+ 'تم نشر هذة المقالة من تطبيق المطوف';
        telegramTxt=   encodeURIComponent(telegramTxt);
        window.open('tg://msg?text=' +telegramTxt, 'href', 'popup');
        return false;
    }
}






