'use strict';

/**
 * @ngdoc function
 * @name hamalatAdminApp.controller:LoginCtrl
 * @description
 * # LoginCtrl
 * Controller of the hamalatAdminApp
 */
hamalatAdminApp
  .controller('dashboardCtrl', ['$scope', '$rootScope', '$http', '$routeParams','$location','User',
    function ($scope,$rootScope, $http,  $routeParams,$location,User) {

      $rootScope.topTitle = 'حملتي';
      $rootScope.logOut= true;
      $rootScope.loading= false;
      $rootScope.backBtn= false;
      $rootScope.mainTitle = User.getUser().hamlaName
    }]);
