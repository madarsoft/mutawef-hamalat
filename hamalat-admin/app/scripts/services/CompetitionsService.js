hamalatAdminApp
  .factory('CompetitionsService',['$http','$rootScope' ,function($http, $rootScope) {

    return{
      addParagraph : function(info) {
        return $http.post($rootScope.domain+ 'HamlaCompetition/create', info);
      } ,

      updateParagraph : function(info) {
        return $http.post($rootScope.domain+'HamlaCompetition/Edit', info);

      },
      removeParagraph : function(paragraphId){
        return $http.post($rootScope.domain+'HamlaCompetition/Delete', paragraphId);
      },
      viewCompetitions : function(info){
        return $http.post($rootScope.domain+'HamlaCompetition/Index', info);
      }
    };



  }]);
