'use strict';

/**
 * @ngdoc service
 * @name core.Services.User
 * @description User Factory
 */

//i need the data of the user to be a service availble on every screen/ and to check loggedin or not
//local storage to set and get the user on logged in
hamalatAdminApp
  .service('User',
  function () {

    var user = {
      id: '',
      name: '',
      email:'',
      phone:'',
      hamlaId:'',
      hamlaName: '',
      hamlaCode: '',
      roleId: '',
      sharedLocation: '',
      status: ''
    };

    this.getUser = function () {

      return(  JSON.parse(localStorage.getItem('hamalat_adminUser')));

    };

    this.setUser = function (id, name, email,phone ,hamlaId, hamlaName, hamlaCode,roleId ,sharedLocation, status) {

      user = {
        id: id,
        name: name,
        email: email,
        phone:phone,
        hamlaId:hamlaId,
        hamlaName: hamlaName,
        hamlaCode: hamlaCode,
        roleId: roleId,
        sharedLocation: sharedLocation,
        status: status
      };
      localStorage.setItem('hamalat_adminUser',JSON.stringify(user));
      return user;
    };




  });
