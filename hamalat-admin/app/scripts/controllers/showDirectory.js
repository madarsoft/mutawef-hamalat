'use strict';

/**
 * @ngdoc function
 * @name hamalatAdminApp.controller:LoginCtrl
 * @description
 * # LoginCtrl
 * Controller of the hamalatAdminApp
 */
hamalatAdminApp
  .controller('showDirectory', ['$scope', '$rootScope', '$http', '$routeParams','$location','directoryService','User',
    function ($scope,$rootScope, $http,  $routeParams,$location, directoryService,User) {


      $rootScope.loading= false;
      $rootScope.backBtn= true;
      $rootScope.logOut= false;
      //$scope.directory= directoryService.getUser();
      $rootScope.mainTitle = 'دليل الحملة';
      $rootScope.topTitle = 'حملتي';

      //$rootScope.directoryData = [
      //  {
      //    id:'',
      //    name:'',
      //    phone:''
      //  }
      //];
      var user = $rootScope.user ;
      if(user != ''){
        $scope.hamlaId = user.hamlaId;
      }



      var campaignId= {
        hamlaId :$scope.hamlaId
      }

      $rootScope.backAction = function () {
        window.location.href ='#/dashboard';
      }
      $scope.initiateDirectory = function () {

        var res = directoryService.showDirectory(campaignId);

        res.success(function (data, status, headers, config) {

          if(data =='server error'){
            $rootScope.message= 'حدث خطأ اثناء الاتصال بالخادم .. من فضلك حاول مرة أخري';
            $('#failPopup').modal('show');
          }
          else if(data.hasOwnProperty('guides')) {
            $rootScope.directoryData = data.guides;
            console.log($rootScope.directoryData)
          }
        });
        res.error(function (data, status, headers, config) {
          $rootScope.message= 'حدث خطأ اثناء الاتصال بالخادم .. من فضلك حاول مرة أخري';
          $('#failPopup').modal('show');
        });

      };





      $scope.updateDirectory =  function () {
        window.location.href = "#/directory";
      };

      $scope.initiateDirectory();

    }]);
