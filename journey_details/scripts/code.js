


var artId = '';
var artDetails = '';
var isMobile = true;
var shareUrl = '';
var shorlSharUrl ='';
var twitterTitle = '';
var iphoneDevice = false;
var androidDevice = false;
var app= false;


var shareCount  = 0;
var likeCount  =0 ;
var commentCount = 0 ;
var likeId = 0;
var reportId = 0;
var userImage= '';
var userId= 0 ;
var userName= '';
var currentOperation = 0 ;
var dataLoaded = false;
var currentItem = '';
var lastCommentId = 0 ;
var offset="00:00:00";



function getUrlVars() {

    var vars = {};
    var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
        vars[key] = value;
    });
    return vars;
}

function init() {


    artId = decodeURI(getUrlVars()["id"]).split("+").join(" ");

    userId = decodeURI(getUrlVars()["userId"]).split("+").join(" ");



    shareUrl = 'http://mutawef.com/share/share/index?id=' + artId
    shorlSharUrl = shareUrl;


    if(navigator.userAgent.match(/Android/) || navigator.userAgent.match(/iPhone/) || navigator.userAgent.match(/iPad/) || navigator.userAgent.match(/iPod/))
    {

        isMobile = true;

    }

    else{

        isMobile = false;
    }
    if( !isMobile){
      
    }

    //
    //if ( navigator.userAgent.match(/iPhone/) || navigator.userAgent.match(/iPad/) || navigator.userAgent.match(/iPod/)) {
    //    iphoneDevice = true;
    //    iOSversion();
    //
    //}else if(navigator.userAgent.match(/Android/) ) {
    //    androidDevice = true;
    //    $('.iframeContainer').css('text-align', 'justify');
    //}
    app = true;

    loadData();
   // loadComments();
}

function iOSversion() {
    var str = navigator.userAgent;
    var n =  str.indexOf("OS");
    n = n + 3;
    str= str.substring(n , str.length)
    str= str.substr(0,str.indexOf(' ')); // "72"
    str= str.substr(str.indexOf(' ')+1);
    str = str.split('_');

    if(parseInt(str[0]) !=10 ||  parseInt(str[1]) !=3 )
    {

        $('.iframeContainer').css('text-align', 'justify');

    }
}




window.loadSocialData = function(userNameParm, userIdParm, userImageParm, likeIdParam ){

    userImage= userImageParm;
    userId = userIdParm  ;
    userName= userNameParm;
    likeId = likeIdParam;


    //alert(userNameParm)
    //alert(userIdParm)
    //alert(userImageParm)

    console.log("omnia        "+userNameParm)
    console.log("omnia        "+userIdParm)
    console.log("omnia        "+userImageParm)


    if(likeId !=0 ){
        $('#like i').addClass('active');
    }else{
        $('#like i').removeClass('active');
    }

    if(userId == 0){
        $('.actions' ).css('visibility', 'hidden');
        dataLoaded = false;
    }else{
        checkUserAuth();
    }


    if(userImageParm != '' && userImageParm != null){
        userImage= userImageParm;
        $('#userImage').attr('src', userImage);
    }else{

        userImage= 'images/userImage.jpg';
        $('#userImage').attr('src', userImage);
    }
    selectOperation(currentOperation);
}


function selectOperation(operation){

    currentOperation = operation;
    $('.commentError').hide();

    if(operation != 0){
        if(userId == 0 ){
            window.location.href = 'loginAction://'
        }else{
            switch (operation) {
                case 1:
                    add_remove_like();    //add or remove like for post
                    break;
                case 2:
                    addComment(); //add  comment for post
                    break;
                case 3:

                    editComment(); //edit  comment for post
                    break;
                case 4:

                    sendEditComment(); //edit  comment for post
                    break;
                case 5:
                    confirmRemove();
                    break;
                case 6:

                    reportArt();
                    break;
            }
        }
    }
}


function loadData(){

    $('.loadingDiv').show();
    var sendData = {
        id:artId
    }

        var res = $.ajax({
            url: 'http://api.mutawef.com/khwater/details',
            type: "POST",
            data: JSON.stringify(sendData),
            datatype: 'json',
            contentType: false,
            processData: false



        })
    res.done(function (data, status, headers, config) {

        $('.loadingDiv').hide();
        offset = data.TimeOffset;
        if (data.hasOwnProperty('text')) {

           $('.iframeContainer').html(data.text);

        }

        if (data.hasOwnProperty('user')) {

            $('#mainUserName').html(data.user.name);
            $('#mainUserImg').attr('src', data.user.image);
        }

        if(data.hasOwnProperty('comments')){

            if(data.comments.length > 0){
                lastCommentId = data.comments[data.comments.length -1].id;
                viewComments(data.comments);
            }


        }

        if (data.hasOwnProperty('date')) {

            $('#artDate').html(date_parse(data.date, offset));

        }
        shareCount = data.sharesCount;
        commentCount = data.commentsCount;
        likeCount = data.likesCount ;
        $('#shareCount span').html(data.sharesCount);
        $('#commentCount span').html(data.commentsCount);
        $('#like span').html(data.likesCount);

        if(data.hasOwnProperty('category')){
            $('.dataContainer').css('border-right', '10px solid '+data.category.color)
        }

    });
    res.fail(function (data, status, headers, config) {


        $('.loadingDiv').hide();
       // $('.commentError').show();

    });


}
function viewComments(comments){


            var item = '' ;

            for (var i = 0 ; i < comments.length ; i++){
                item = comments[i];
                var listItem =
                    '<div class="my-listItem" id="user'+item.userId+'">'+
                        '<img  src="'+item.image+'"   >'+
                        '<div class="commentTextContent" >'+
                            '<p class="userName">'+item.name+'</p>'+
                            '<p class="userComment">'+item.text.replace(/\r?\n/g, '<br/>')+'</p>'+
                            '<div class="actions" id="comment'+item.id+'">'+
                                '<span>'+
                                date_parse(item.date, offset)+
                                '</span>'+
                                '<a class="commentAction editAction" >.تعديل</a>'+
                                '<a class="commentAction removeAction">.حذف</a>'+
                                '<a class="commentAction loadingAction"> <img  class="smallLoad" id="removeLoading"  style="display: block" src="images/loading.gif" ></a>'+

                            '</div>'+

                        '</div>'+
                '</div>';
                $('#comments').append(listItem);
                if(userId == item.userId){
                    $('#comment'+item.id).css('visibility', 'visible');
                }



            }




}

function loadMoreComments(){

    $('#loadMorComments').show();
    var sendData = {
        khatraId:artId,
        commentId:lastCommentId.toString(),
        count:'10'
    }


    var res = $.ajax({
        url: 'http://api.mutawef.com/Comment/Paging',
        type: "POST",
        data: JSON.stringify(sendData),
        datatype: 'json',
        contentType: false,
        processData: false


    })
    res.done(function (data, status, headers, config) {


        $('#loadMorComments').hide();

        if(data.hasOwnProperty('comments')){

            if(data.comments.length > 0){
                lastCommentId = data.comments[data.comments.length -1].id;
                viewComments(data.comments);
            }else{
                lastCommentId = 0 ;
            }


        }




    });
    res.fail(function (data, status, headers, config) {

        $('#loadMorComments').hide();

        // $('.commentError').show();

    });
}




function checkUserAuth(){
    var accountId = '';
    var commentId ='' ;
    $( ".my-listItem" ).each(function( i ) {
        accountId = $(this).attr('id');
        commentId = $(this).find( ".actions").attr('id');
        if(accountId == "user"+userId) {

            $('#' + commentId).css('visibility', 'visible');


        }else{
            $('#' + commentId).css('visibility', 'hidden');
        }
    });
}



function addComment(){


    var text = $('#commentValue').val().trim();
    if(text != '' ){
        sendComment(text);
    }


}
function sendComment(comment){

    var sendData =
    {
        khatraId:artId,
        accountId:userId.toString(),
        comment:comment
    }


    $('#sendComment').hide();
    $('#commentLoad').show();


    var res = $.ajax({
        url: 'http://api.mutawef.com/Comment/Add',
        type: "POST",
        data: JSON.stringify(sendData),
        datatype: 'json',
        contentType: false,
        processData: false


    })
    res.done(function (data, status, headers, config) {

        $('#commentLoad').hide();

        if (data.hasOwnProperty('id')) {

                //commentCount = data.commentsCount;

                $('#sendComment').show();
                $('#commentValue').val('');
                $('#commentValue').css('height', 'auto' );
                //$('#comment span').html(commentCount);

                var  id= data.id;

            var date = date_parse(new Date().getTime()/1000, '00:00:00')
                var item =
                    '<div class="my-listItem" id="user'+userId+'">'+
                        '<img  src="'+userImage+'"   >'+
                        '<div class="commentTextContent" >'+
                            '<p class="userName">'+userName+'</p>'+
                            '<p class="userComment">'+comment.replace(/\r?\n/g, '<br/>') +'</p>'+

                            '<div class="actions" id="comment'+id+'">'+
                                '<span>'+
                                date+
                                '</span>'+
                                '<a class="commentAction editAction" >.تعديل</a>'+
                                '<a class="commentAction removeAction">.حذف</a>'+
                                '<a class="commentAction loadingAction"> <img  class="smallLoad" id="removeLoading"  style="display: block" src="images/loading.gif" ></a>'+

                            '</div>'+

                        '</div>'+
                    '</div>';

                commentCount++;
                $('#commentCount span').html(commentCount);
                $('#comments').prepend(item)
                $('#comment' + id).css('visibility', 'visible');

                window.location.href = 'increaseComments://';


        } else {

            $('#sendComment').show();
            $('.commentError').show();

        }


    });
    res.fail(function (data, status, headers, config) {

        $('#commentLoad').hide();
        $('#sendComment').show();
        $('.commentError').show();

    });

}


function editComment(){
    var index = $( '.editAction' ).index( $(currentItem) );
    var text = $('.userComment:eq( '+index+' )').html();

    currentIndex = index;
    $('#commentValue').val(text.replace(/<br\s*\/?>/mg,"\n"));
    textAreaAdjust(  document.getElementById('commentValue'))
    window.scrollToComments();
    editMode();


}

function sendEditComment(){

    var comment = $('#commentValue').val();
    if(comment != '' ) {

        var commentId = $(currentItem).parents($('.actions')).attr('id');
        commentId = commentId.substring(7, commentId.length);
        var comment_data =
        {
            id:commentId,
            comment:comment
        }

        $('#sendEdit').hide();
        $('#commentLoad').show();


        var res = $.ajax({
            url: 'http://api.mutawef.com/Comment/Edit',
            type: "POST",
            data: JSON.stringify(comment_data),
            datatype: 'json',
            contentType: false,
            processData: false


        })
        res.done(function (data, status, headers, config) {

            $('#commentLoad').hide();

            if (data.hasOwnProperty('result')) {
                if (data.result == true) {
                    //commentCount = data.commentsCount;

                    sendMode();
                    $('#commentValue').val('');
                    $('#commentValue').css('height', 'auto');
                    $('.userComment:eq( '+currentIndex+' )').html(comment_data.comment.replace(/\r?\n/g,'<br/>'));
                    $('html,body').animate({
                            scrollTop: $('.userComment:eq( '+currentIndex+' )').offset().top -40
                        },
                        'slow');
                    //$('#comment span').html(commentCount);



                } else {
                    sendMode();
                    $('.commentError').show();

                }
            } else {

                sendMode();
                $('.commentError').show();

            }


        });
        res.fail(function (data, status, headers, config) {

            $('#commentLoad').hide();
            sendMode();
            $('.commentError').show();

        });

    }
}






function confirmRemove(){
    //$('.confirmRemove').show();
    $('.confirmRemove').slideDown();
}

function removeComment(){
    var index = $( '.removeAction' ).index( $(currentItem) );
    currentIndex = index;

    showRemoveLoading();

    var commentId = $(currentItem).parents($('.actions')).attr('id');
    commentId = commentId.substring(7, commentId.length);
    var comment_data =
    {id:commentId.toString()}






    var res = $.ajax({
        url: 'http://api.mutawef.com/Comment/Delete',
        type: "POST",
        data: JSON.stringify(comment_data),
        datatype: 'json',
        contentType: false,
        processData: false


    })
    res.done(function (data, status, headers, config) {

        hideRemoveLoading();

        if (data.hasOwnProperty('result')) {

            if(data.result) {

                $('#comments .my-listItem').eq(currentIndex).remove();
                commentCount--;
                $('#commentCount span').html(commentCount);
                window.location.href = 'decreaseComments://';
            }else{
                $('.commentError').show();
            }

        } else {


            $('.commentError').show();

        }


    });
    res.fail(function (data, status, headers, config) {

        showRemoveLoading();
        $('.commentError').show();

    });
}

function showRemoveLoading (){
    $('.removeAction').eq(currentIndex).hide();
    $('.loadingAction').eq(currentIndex).css('display','inline-block');
}

function hideRemoveLoading (){
    $('.loadingAction').eq(currentIndex).css('display','none');
    $('.removeAction').eq(currentIndex).show();

}



window.scrollToComments = function() {
    setTimeout(function() { $('#commentValue').focus(); }, 1000);
    $('html,body').animate({
            scrollTop: $("#comments" ).offset().top -20
        },
        'slow');



}

function shareDetails(){
   window.location.href = 'shareJourney://' ;

}


function add_remove_like(){

    if(likeId == 0)
    {
        addLike();
    }else{
        unLike();
    }
}
function addLike() {

    var send_data = {
        khatraId:artId,
        userId:userId.toString()
    }
    $('#like i').hide();
    $('#likeLoad').show();


    var res = $.ajax({
        url: 'http://api.mutawef.com/like/add',
        type: "POST",
        data: JSON.stringify(send_data),
        datatype: 'json',
        contentType: false,
        processData: false


    })
    res.done(function (data, status, headers, config) {

        $('#likeLoad').hide();
        if (data.hasOwnProperty('result')) {

                likeId = 1;
                likeCount = data.result;
                $('#like i').addClass('active');
                $('#like i').show();
                $('#like span').html(likeCount);



        }
        else {
            $('#like i').show();

        }

    });
    res.fail(function (data, status, headers, config) {

        $('#likeLoad').hide();
        $('#like i').show();


    });

}

function unLike() {

    var send_data = {
        khatraId:artId,
        userId:userId.toString()
    }
    $('#like i').hide();
    $('#likeLoad').show();


    var res = $.ajax({
        url: 'http://api.mutawef.com/like/UnLike',
        type: "POST",
        data: JSON.stringify(send_data),
        datatype: 'json',
        contentType: false,
        processData: false


    })
    res.done(function (data, status, headers, config) {

        $('#likeLoad').hide();
        if (data.hasOwnProperty('result')) {

                likeCount = data.result;
                likeId = 0;
                $('#like i').removeClass('active');
                $('#like i').show();
                $('#like span').html(likeCount);

        }else {

            $('#like i').show();

        }


    });
    res.fail(function (data, status, headers, config) {

        $('#likeLoad').hide();
        $('#like i').show();

    });

}




    function reportArt() {

        var send_data = {
            khatraId:artId,
            commentId:'0',
            accountId:userId.toString(),
            text:'report'

        }
        $('#report i').hide();
        $('#reportLoad').show();


        var res = $.ajax({
            url: 'http://api.mutawef.com/Report/Index',
            type: "POST",
            data: JSON.stringify(send_data),
            datatype: 'json',
            contentType: false,
            processData: false


        })
        res.done(function (data, status, headers, config) {

            $('#reportLoad').hide();
            if (data.hasOwnProperty('result')) {

                if(data.result) {
                    reportId = 1;

                   // $('#report i').addClass('active');
                    $('#report i').show();


                }


            }
            else {
                $('#report i').show();

            }

        });
        res.fail(function (data, status, headers, config) {

            $('#reportLoad').hide();
            $('#report i').show();


        });

    }
window.getCommentsCount = function() {
    var count = $('.my-listItem').length;
    return count;
}

function sendMode(){
    $('#sendEdit').hide();
    $('#sendComment').show();
}
function editMode(){
    $('#sendComment').hide();
    $('#sendEdit').show();
}

$('body').on('click', '.editAction', function() {
    // do something
    currentItem = this;
    selectOperation(3)
});

$('body').on('click', '.removeAction', function() {
    // do something
    currentItem = this;
    selectOperation(5)
});
$('body').on('click', '.closePopup', function() {
    // do something
    $('.confirmRemove').slideUp();
});

$('body').on('click', '.redButton', function() {
    // do something
    $('.confirmRemove').slideUp();
    setTimeout(function(){
        removeComment();
    }, 500)

});

$(window).scroll(function() {
    if(lastCommentId != 0) {
        if ($(window).scrollTop() + $(window).height() == $(document).height()) {
            loadMoreComments();
        }
    }
});

function facebookShare(){

    window.open("http://www.facebook.com/sharer.php?u="+encodeURIComponent(shareUrl),'_system', 'location=no');


}
function twitterShare() {

    var twitterTxt =newsDetails.title + '\r\n' +shorlSharUrl;
    twitterTxt=   encodeURIComponent(twitterTxt);
//http://stackoverflow.com/questions/23095906/how-to-open-twitter-and-facebook-app-with-phonegap
//http://wiki.akosma.com/IPhone_URL_Schemes#Twitter
    // If Mac//


    if (iphoneDevice && app) {
        setTimeout(function () {
                window.open('https://itunes.apple.com/ca/app/twitter/id333903271?mt=8', '_system', 'location=no'); }
            ,25);
        window.open('twitter://post?message=' + twitterTxt, '_system', 'location=no');

    }

//If Android

    else {

        //window.open( 'https://twitter.com/intent/tweet?url='+twitterTxt+'&text='+$scope.twitterTitle , 'href','popup');
        window.open('https://twitter.com/share?url='+escape(shorlSharUrl)+'&text='+'\r\n' +encodeURIComponent(newsDetails.title )+'\r\n'  , '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,width=600');
        return false;
        return false;
    }
}




function whatsappShare(){
    //if (androidDevice && app) {
    //    // window.location.href = 'whatsappShare://';
    //}else{
        var whatsappTxt =newsDetails.title + '\r\n' + newsDetails.abbrev + '\r\n' + shorlSharUrl +'\r\n'+'تم نشر هذة المقالة من تطبيق المطوف';

        whatsappTxt=   encodeURIComponent(whatsappTxt);
        window.open('whatsapp://send?text='+ whatsappTxt , 'href', 'popup');
        return false;
    //}

}
function mailShare(){
    //if (androidDevice && app) {
    //    // window.location.href = 'mailShare://';
    //}else {
        var subject = newsDetails.title;
        var emailBody ='<p>'+newsDetails.title +'</p>'+ '\r\n' + '<p>'+newsDetails.abbrev +'</p>'+'\r\n' +'\r\n' +shorlSharUrl + '\r\n' +'\r\n'+'<img src="'+newsDetails.main_image +'" width="100%">'+ '\r\n'+'تم نشر هذة المقالة من تطبيق المطوف';
        emailBody=   encodeURIComponent(emailBody);
        window.open('mailto: ?subject=' + subject + '&body=' + emailBody, 'href', 'popup');
        return false;
   // }
}


function telegramShare(){
    //if (androidDevice && app) {
    //    // window.location.href = 'telegramShare://';
    //}else {
        var telegramTxt =newsDetails.title + '\r\n' + newsDetails.abbrev + '\r\n' + shorlSharUrl +'\r\n'+ 'تم نشر هذة المقالة من تطبيق المطوف';
        telegramTxt=   encodeURIComponent(telegramTxt);
        window.open('tg://msg?text=' +telegramTxt, 'href', 'popup');
        return false;
   // }
}


function textAreaAdjust(o) {
    o.style.height = "1px";
    o.style.height = (o.scrollHeight)+"px";
    $('.sentBtn').css('padding-left','10px');
}

function date_parse(date, timezone) {

    // Create a new JavaScript Date object based on the timestamp
    // multiplied b y 1000 so that the argument is in milliseconds, not seconds.


    var temp = timezone.split(':');
    timezone = temp[0].substr(-1);
    if(temp[2].length >2){
        timezone = timezone *- 1;
    }


    timezone = timezone*60*60

    var date = new Date((date - timezone)*1000);
    // Hours part from the timestamp
    var hours = date.getHours();
    // Minutes part from the timestamp
    var minutes = "0" + date.getMinutes();
    // Seconds part from the timestamp
    var seconds = "0" + date.getSeconds();


    var am_pm = 'AM' ;


    if(hours > 12 ){
        hours = hours -12 ;
        am_pm = 'PM';
    }else if(hours == 12 ){
        am_pm = 'PM';
    }else if(hours == 0 || hours == 24){
        hours = 12 ;

    }

    // day part from the timestamp
    var day =  ""  + date.getDate();
    // Month part from the timestamp
    var month = "" + (date.getMonth()+1);
    // Year part from the timestamp
    var year =  date.getFullYear();

    // Will display time in 10:30:23 format
    var formattedTime = am_pm +' '+ hours + ':' + minutes.substr(-2)
    //console.log(formattedTime)

    var formattedDate= year + '/' + month.substr(-2) + '/' + day.substr(-2);
    //console.log(formattedDate)

    //return formattedDate+'  ' + formattedTime
    return formattedDate+"  -  "+formattedTime
}