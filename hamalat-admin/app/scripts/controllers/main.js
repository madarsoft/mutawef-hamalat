'use strict';

/**
 * @ngdoc function
 * @name hamalatAdminApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the hamalatAdminApp
 */
angular.module('hamalatAdminApp')
  .controller('MainCtrl', ['$scope', '$rootScope', '$http', '$routeParams','$location','User',
    function ($scope,$rootScope, $http,  $routeParams,$location, User) {



      if ($routeParams.myDay === undefined || $routeParams.myDay === null || parseInt($routeParams.myDay) > 8 || parseInt($routeParams.myDay )< 1){
        // do something
        $rootScope.realDay = 1;

      }else{
        $rootScope.realDay = $routeParams.myDay;
      }

      $rootScope.topTitle = 'حملتي';
      $rootScope.mainTitle = 'حملات';
      $rootScope.loading= true;
      $rootScope.logOut= false;
      $rootScope.backBtn= false;
      $rootScope.message='';
      $rootScope.domain = 'http://api.mutawef.com/';
      $rootScope.directoryData= [];

      setTimeout(function(){ checkUserStatus(); }, 100);

      function checkUserStatus() {
        $rootScope.user = '';
        var user = User.getUser();

        if (user != null && user.status === 'loggedIn') {
          $rootScope.user = user;
          $rootScope.mainTitle = user.hamlaName
          window.location.href ='#/dashboard';
        }else{
          window.location.href ='#/login';
        }

      }
      $rootScope.okOnPopup = function () {

      }
      $rootScope.backAction = function () {
        window.location.href ='#/dashboard';
      }

      $rootScope.okDialogPopup = function () {

      }
      $rootScope.cancelDialogPopup = function () {

      }

      $rootScope.logoutUser = function(){

        User.setUser('', '','', '' ,'', '', '', 'loggedOut');
        $rootScope.user = User.getUser();
        window.location.href = "#/main";
      }

      checkUserStatus();

    }]);
