hamalatAdminApp
  .factory('ScheduleService',['$http', '$rootScope', function($http, $rootScope) {

    return{
      addProgram : function(info) {
        return $http.post($rootScope.domain+'HamlaProgram/Create', info);
      } ,
      getProgram : function(info){
        return $http.post($rootScope.domain+'HamlaProgram/Details', info)
      },
      updateProgram : function(info) {
        return $http.post($rootScope.domain +'HamlaProgram/Edit', info);

      },
      removeProgram : function(info){
        return $http.post($rootScope.domain+ 'HamlaProgram/Delete', info);
      },
      viewSchedule : function(info){
        return $http.post($rootScope.domain+ 'HamlaProgram/index', info);
      }
    };



  }]);
