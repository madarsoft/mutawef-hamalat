'use strict';

/**
 * @ngdoc function
 * @name hamalatAdminApp.controller:LoginCtrl
 * @description
 * # LoginCtrl
 * Controller of the hamalatAdminApp
 */
hamalatAdminApp
  .controller('newsCtrl', ['$scope', '$rootScope', '$http', '$routeParams','$location','NewsService',
    function ($scope,$rootScope, $http,  $routeParams,$location, NewsService) {


      $rootScope.topTitle = 'حملتي';
      $rootScope.backBtn= true;
      $rootScope.message = '';
      $rootScope.loading= false;
      $rootScope.logOut= false;
      $rootScope.mainTitle= 'أخبارنـــا';
      var user = $rootScope.user ;



      if(user != ''){
        $scope.hamlaId = user.hamlaId;

      }




      $scope.newsList = [];
      $scope.editeText ='';
      $scope.inputParagraph = {
        text:''
      };
      $scope.removeIndex = 0;

      //////////////////////////////////////////////////////////////////





      $scope.loadDay = function() {

        $scope.newsList=[]
        // alert($scope.myDayIndex)
        for (var i = 0; i <  10; i++) {
          var p = {
            id: i + 1,
            text: 'paragraph' + (i + 1),
            edit: false
          }


            $scope.newsList.push(p);



        }





      }
      //$scope.loadDay();
      /////////////////////////////////////////////////////////////



      $rootScope.backAction = function () {
        window.location.href ='#/dashboard';
      }

      $scope.getNews = function () {
        $scope.newsList = [];

          var info ={
            hamlaId :$scope.hamlaId

          }

        $rootScope.loading= true;
          var res =  NewsService.viewNews(info)
          res.success(function (data) {

            $rootScope.loading= false;
            if(data.hasOwnProperty('hamlaNews')){

              var list = data.hamlaNews;
              for(var i = 0; i < list.length; i ++){
                var temp = {
                  id:list[i].newsId,
                  text:list[i].newsText,
                  edit:false
                }

                $scope.newsList.unshift(temp);// to push in an array from the top

              }
            }
            else {

              $rootScope.message= 'حدث خطأ اثناء الاتصال بالخادم .. من فضلك حاول مرة أخري';
              $('#failPopup').modal('show');

            }
          });
          res.error(function (err) {

            $rootScope.loading= false;
            $rootScope.message = 'حدث خطأ اثناء الاتصال بالسيرفر .. من فضلك حاول مرة أخري';

            $('#failPopup').modal('show');
          });





      }

      $scope.changeToEditMode= function (index){

        $scope.newsList[index].edit= true;
        $scope.editeText =  $scope.newsList[index].text;
        $scope.newsList[index].text = $scope.newsList[index].text.replace(/<br\s*\/?>/mg,"\n");
        $('.stepInput textarea').height('95%')
        $('.stepInput textarea').css('min-height', '150px');
        $scope.closeOtherEdits(index);
      }

      $scope.closeOtherEdits = function(index){
        for(var i =0; i < $scope.newsList.length ; i ++){
          if(i != index) {
            $scope.newsList[i].edit = false;
          }
        }
      }


      $scope.cancelEdit= function (index){


        $scope.newsList[index].text = $scope.editeText;

        $scope.newsList[index].edit= false;
      }




      $scope.addParagraph= function (index){

/////////////////////////  to be removed for test //////////////////////////////////

        var temp = {
          hamlaId :$scope.hamlaId,
          text:$scope.inputParagraph.text.replace(/\r?\n/g,'<br/>')
        }


        $rootScope.loading= true;

        var res =  NewsService.addParagraph(temp)
        res.success(function (data) {
          $rootScope.loading= false;


          if(data.hasOwnProperty('newsId')){


            var temp = {
              id:data.newsId,
              text:$scope.inputParagraph.text.replace(/\r?\n/g,'<br/>'),
              edit:false
            }

            $scope.newsList.unshift(temp);
            $scope.inputParagraph.text = '';

            $rootScope.message= 'تم اضافة خبـــر بنجاح';
            $('#successPopup').modal('show');


          }else{
            $rootScope.message= 'حدث خطأ اثناء الاتصال بالخادم .. من فضلك حاول مرة أخري';
            $('#failPopup').modal('show');
          }

        });
        res.error(function (err) {
          $rootScope.loading= false;
          $rootScope.message = 'حدث خطأ اثناء الاتصال بالسيرفر .. من فضلك حاول مرة أخري';

          $('#failPopup').modal('show');
        });

      }


      $scope.saveEdit= function (index){
        var currentParagraph =  $scope.newsList[index];
        var info ={
          newsId :(currentParagraph.id).toString(),
          text :currentParagraph.text.replace(/\r?\n/g,'<br/>')

        }



        $rootScope.loading= true;
        var res =  NewsService.updateParagraph(info)
        res.success(function (data) {
          $rootScope.loading= false;


          if (data.hasOwnProperty('result')) {
            if(data.result.hasOwnProperty('serverError'))
            {
              $rootScope.message= 'حدث خطأ اثناء الاتصال بالخادم .. من فضلك حاول مرة أخري';
              $('#failPopup').modal('show');
            }
            else if(data.result == true){
              $scope.newsList[index].text = $scope.newsList[index].text.replace(/\r?\n/g,'<br/>');
              $scope.newsList[index].edit= false;
              $rootScope.message= 'تم حفظ التعديلات بنجاح';
              $('#successPopup').modal('show');
            }else{
              $rootScope.message= 'حدث خطأ اثناء الاتصال بالخادم .. من فضلك حاول مرة أخري';
              $('#failPopup').modal('show');
            }

          }

        });
        res.error(function (err) {
          $rootScope.loading= false;
          $rootScope.message = 'حدث خطأ اثناء الاتصال بالسيرفر .. من فضلك حاول مرة أخري';

          $('#failPopup').modal('show');
        });

      }

      $scope.confirmRemoveParagraph= function (index){

        $rootScope.removeIndex = index;
        $rootScope.message = 'هل تريد فعلا مســح الخـبـر';
        $('#dialogPopup').modal('show');

      }
      $scope.removeParagraph= function (){

        var index =$rootScope.removeIndex;
        var id= {
          newsId: ($scope.newsList[index].id).toString()
      }

        $rootScope.loading= true;
        var res =  NewsService.removeParagraph( id)
        res.success(function (data) {
          $rootScope.loading= false;
          if (data.hasOwnProperty('result')) {
            if(data.result.hasOwnProperty('serverError'))
            {
              $rootScope.message= 'حدث خطأ اثناء الاتصال بالخادم .. من فضلك حاول مرة أخري';
              $('#failPopup').modal('show');
            }
            else if(data.result == true){
              $scope.newsList.splice(index, 1);
              $rootScope.message= 'تم مســح الخــبر بنجاح';
              $('#successPopup').modal('show');

            }else{
              $rootScope.message= 'حدث خطأ اثناء الاتصال بالخادم .. من فضلك حاول مرة أخري';
              $('#failPopup').modal('show');
            }

          }

        });
        res.error(function (err) {
          $rootScope.loading= false;
          $rootScope.message = 'حدث خطأ اثناء الاتصال بالسيرفر .. من فضلك حاول مرة أخري';

          $('#failPopup').modal('show');
        });
      }



      $rootScope.cancelDialogPopup = function () {

        $('#dialogPopup').modal('hide');
        $scope.removeIndex = 0;
      }

      $rootScope.okDialogPopup = function () {

        $('#dialogPopup').modal('hide');
        $scope.removeParagraph();
      }

      $scope.getNews();




    }]);
