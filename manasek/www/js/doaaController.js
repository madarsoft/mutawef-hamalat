function doaaController() {
    var doaaList2 = new Array();
};

doaaController.prototype.retrieveFromXML = function () {
    var myobj = this;
    myobj.DoaaView = new doaaView();
    var doaaList = new Array();

    function doaa(text, source) {
        this.text = text;
        this.source = source;

    }

    var randomNumber = Math.floor(Math.random() * 14) + 1;
   
    $.ajax({
        url: 'xml/doaa'+randomNumber+'.xml',
        dataType: 'xml',
        timeout: 60000,
        type: 'GET',
        cache:'false',
        success: function (data) {
            $(data).find("doaa").each(function () {
                var doaaText = $(this).find("text").text();
                var source = $(this).find("source").text();
                var newListItem = new doaa(doaaText, source);

                doaaList.push(newListItem);

            })

            myobj.DoaaView.showDoaa(doaaList);

        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            //Message('#note_popup',"Error In getCachedCities : " + err.message);

        }
    });
}