'use strict';

/**
 * @ngdoc function
 * @name hamalatAdminApp.controller:LoginCtrl
 * @description
 * # LoginCtrl
 * Controller of the hamalatAdminApp
 */
hamalatAdminApp
  .controller('LoginCtrl', ['$scope', '$rootScope', '$http', '$routeParams','$location','User','UserService',
    function ($scope,$rootScope, $http,  $routeParams,$location, User,UserService) {

      $rootScope.topTitle = 'حملتي';
      $rootScope.mainTitle = 'تسجيل دخول' ;
      $rootScope.loading= false;
      $rootScope.backBtn= false;
      $rootScope.logOut= false;

      $scope.user = {
        userEmail: '',
        userPassword: '',
        udid: ''
      };


      $scope.loginAdmin = function (isValid) {

       if(isValid) {

         var userData = {
           email: $scope.user.userEmail,
           password: $scope.user.userPassword
         };

         $rootScope.loading= true;
         var res = UserService.login(userData);

         res.success(function (data) {
           $rootScope.loading= false;

           if (data.hasOwnProperty('userInfo')) {
              if(data.userInfo == 0){
                $rootScope.message = 'البريد الإلكتروني او كلمة المرور غير صحيحة';
                $('#failPopup').modal('show');
              }else{

                if(data.userInfo.sharedLocation == null)
                {
                  data.userInfo.sharedLocation = false;
                }
                User.setUser(data.userInfo.userId, data.userInfo.userName, userData.email,data.userInfo.phone
                  ,data.userInfo.campaignId, data.userInfo.hamalName, data.userInfo.code,
                  data.userInfo.roleId , data.userInfo.sharedLocation,'loggedIn');
                $rootScope.user = User.getUser();
                console.log($rootScope.user )
                window.location.href = "#/dashboard";
              }
           }

           else if (data.hasOwnProperty('serverError')) {
             $rootScope.message = 'حدث خطأ اثناء الاتصال بالسيرفر .. من فضلك حاول مرة أخري';
             $('#failPopup').modal('show');
           }

         });
         res.error(function (err) {
           $rootScope.loading= false;
           $rootScope.message = 'حدث خطأ اثناء الاتصال بالسيرفر .. من فضلك حاول مرة أخري';
           $('#failPopup').modal('show');
         });
       }

      };




      $scope.newCampaign = function (){
        window.location.href = "#/register";
      };

      $scope.forgotPassword = function (){
        window.location.href = "#/forgotMyPassword";
      };

    }]);
