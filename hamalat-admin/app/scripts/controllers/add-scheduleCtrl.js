'use strict';

/**
 * @ngdoc function
 * @name hamalatAdminApp.controller:LoginCtrl
 * @description
 * # LoginCtrl
 * Controller of the hamalatAdminApp
 */
hamalatAdminApp
  .controller('add-scheduleCtrl', ['$scope', '$rootScope', '$http', '$routeParams','$location','ScheduleService','imageService','timeService',
    function ($scope,$rootScope, $http,  $routeParams,$location, ScheduleService, imageService, timeService) {


      $rootScope.loading= false;
      $rootScope.backBtn= true;
      $rootScope.logOut= false;
      $rootScope.message = '';
      $rootScope.topTitle = 'حملتي';

      $('header h3').html('إضافة برنامج للحملة' )



     // $rootScope.loading= true;
      var user = $rootScope.user ;
      $scope.myDayIndex =$rootScope.realDay - 1;
      $scope.uploadImage = false;


      $scope.days =[
        {id:0, name:'ما قبل التروية', loaded :false},
        {id:1, name:'الثامن من ذى الحجة(التروية)', loaded :false},
        {id:2, name:'التاسع من ذي الحجة (يوم عرفة)', loaded :false},
        {id:3, name:'العاشر من ذي الحجة (يوم النحر)', loaded :false},
        {id:4, name:'أول أيام التشريق', loaded :false},
        {id:5, name:'ثانى أيام التشريق', loaded :false},
        {id:6, name:'ثالث أيام التشريق', loaded :false},
        {id:7, name:'اليوم الأخير', loaded :false}
      ];


      if(user != ''){
       $scope.hamlaId = user.hamlaId;

      }


      $scope.program ={
        id:$routeParams.programId,
        day: $scope.realDay,
        title :'',
        details: '',
        image:'',
        lat:'',
        long:'',
        phone:'',
        name :'',
        time:'',
        validTime:false,
        address:'تحديد الموقع علي الخريطة'
      }
debugger
    ///////////////////////////////////////////////////////////////

      $rootScope.backAction = function () {
        window.location.href ='#/dashboard';
      }

      $scope.init = function() {

        if($scope.program.id > 0 ){
          $scope.loadForUpdate();
        }
      }


      $scope.loadForUpdate = function() {
        var info ={
          programId :$scope.program.id

        }
        $rootScope.loading= true;
        var res =  ScheduleService.getProgram(info)
        res.success(function (data) {
debugger
          $rootScope.loading= false;
          if (data.hasOwnProperty('result')) {
            if(data.result.hasOwnProperty('serverError'))
            {
              $rootScope.message= 'حدث خطأ اثناء الاتصال بالخادم .. من فضلك حاول مرة أخري';
              $('#failPopup').modal('show');
            }

          }

          else if(data.hasOwnProperty('hamlaPrograms')){

            if (data.hamlaPrograms[0]){
              var temp = data.hamlaPrograms[0];
                //$scope.$apply(function(){

                $scope.program.day = temp.dayId;
                $scope.myDayIndex = temp.dayId -1;
                $scope.program.title = temp.title;
                $scope.program.details = temp.programInfo;
                $scope.program.name = temp.responserName;
                $scope.program.phone = temp.responserPhone;
                $scope.program.image =temp.imageUrl.substring(temp.imageUrl.lastIndexOf('/')+1, temp.imageUrl.length);
                if( $scope.program.image != '' ){

                  $('.imageContainer').addClass('attachImage');
                  $('#mainImage').attr('src',
                    temp.imageUrl.substring(0, temp.imageUrl.lastIndexOf('/')+1)+$scope.program.image);
                }
                if(temp.lat != null && temp.lng != null){
                  $scope.program.lat = temp.lat;
                  $scope.program.long = temp.lng;
                }else{
                  $scope.program.lat = '';
                  $scope.program.long = '';
                }
              $('.time-picker input').val('4:25 AM');
              $('.time-picker .container').addClass('noError');
             // })


            }


          }
        });
        res.error(function (err) {

          $rootScope.loading= false;
          $rootScope.message= 'حدث خطأ اثناء الاتصال بالخادم .. من فضلك حاول مرة أخري';
          $('#failPopup').modal('show');
        });

      }


      $scope.init();
      //////////////////////    show map ///////////////////////////////////

      $scope.loadMap = function (){

        if($scope.program.lat == '' || $scope.program.long == ''){
          $scope.map(21.422510,39.826168,'الحـــرم')
        }else{
          $scope.map($scope.program.lat,$scope.program.long,'موقع البرنامج');
        }

      }
      $scope.map = function(lat ,long, mapTitle) {

        $scope.program.lat = lat;
        $scope.program.long = long;
        $('#mapPopUp').modal('show');
        $('#mapTitle').html(mapTitle);
        var mapOptions = {
          zoom: 15,
          center: new google.maps.LatLng(lat, long)
        };
        var map = new google.maps.Map(document.getElementById('map'),
          mapOptions);


        var myLatlng = new google.maps.LatLng(lat, long);
        var marker = new google.maps.Marker({
          position: myLatlng,
          map: map

        });
        var infowindow =new google.maps.InfoWindow();
        infowindow.setContent(mapTitle);
        infowindow.open(map, marker);

        google.maps.event.addListener(map,'click',function(event) {
          var lat = event.latLng.lat();
          var lng = event.latLng.lng();

          var myLatlng = new google.maps.LatLng(lat, lng);



         changePosition(marker, infowindow,myLatlng);

        }); // end event

        setTimeout(function(){

          google.maps.event.trigger(map, 'resize');
          map.setZoom( map.getZoom() );
        }, 1000)



      }

      function changePosition(marker, infowindow, myLatlng){

        marker.setPosition(myLatlng);
        infowindow.setContent("مكان البرنامج");
       // infowindow.open(map, marker);
        geocodePosition(marker.getPosition())
      }



      function geocodePosition(pos) {

        var geocoder = new google.maps.Geocoder;

        geocoder.geocode({'location': pos}, function(results, status) {

          if (status === 'OK') {
            if (results[0]) {

              $scope.program.lat = pos.lat();
              $scope.program.long = pos.lng();
              var address = results[0].formatted_address;
              address = address.replace(/\d+/g, '');
              address = address.replace(/،/g, '')
              //var temp = '' ;
              //for(var i = address.length-3 ; i>= 0 ;i -- )
              //{
              //  temp = temp+address[i]
              //}


              $scope.program.address = address;
            } else {
              $scope.program.lat = '';
              $scope.program.long = '';
              $scope.program.address = 'لم يتم تحديد العنوان';
            }
          } else {
            $scope.program.lat = '';
            $scope.program.long = '';
            $scope.program.address = 'لم يتم تحديد العنوان';
          }
        });
      }
      /////////////////////////////////////////////////////////////


      ///////////////////////////////////Image Actions////////////////////////////////////////////

      $scope.addImage = function(input){


        // $scope.files = input.files
        if (input.files && input.files[0]) {

          if(!imageService.isImage(input)) {
            $rootScope.$apply(function() {
              $rootScope.message = 'فضلا تأكد أن نوع الملف المرفوع صورة , و لا يزيد حجم الصورة عن 4 ميجا';
            })
            $('#failPopup').modal('show')
          }
          else if (input.files[0].size/1000000 > 4) {
            $rootScope.$apply(function() {
              $rootScope.message = 'فضلا تأكد أن نوع الملف المرفوع صورة , و لا يزيد حجم الصورة عن 4 ميجا';
            });
            $('#failPopup').modal('show');
          }else{
            var reader = new FileReader();

            reader.onload = function (e) {


              $('.imageContainer').addClass('attachImage');
              $('#mainImage').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
            $scope.program.image =  input.files[0];
            $scope.uploadImage =true;
          }
        }
      }


      $scope.removeImage = function(input){
        $(input).value  = '';

        $('#mainImage').attr('src', '');
        $('.imageContainer').removeClass('attachImage');
        var id =  $(input).attr('id')
        id= id.substring(id.length-1 ,id.length)

        $scope.program.image  = "";
        $scope.uploadImage =false;
      }

      $scope.hasImages = function(isValid){

        $scope.timeValidation();
        if(isValid && $scope.program.validTime ) {
          if ($scope.uploadImage == true) {
            $scope.uploadImages();

          } else {
            $scope.add_update_ProgramFun();

          }
        }
      }


      $scope.uploadImages = function(){


        $rootScope.loading = true;
        var formDataIndex = 0;
        var formData = new FormData();

        formData.append("fileImage", $scope.program.image);




        var res = imageService.UploadMultipleImages(formData);

        res.done(function (data) {


          if(data.hasOwnProperty('img')) {
            $scope.program.image = data.img;

            $scope.add_update_ProgramFun();
          }else{
            $scope.$apply(function(){
              $rootScope.loading= false;
              $rootScope.message= 'حدث خطأ اثناء الاتصال بالخادم .. من فضلك حاول مرة أخري';
              $('#failPopup').modal('show');
            })
          }

        })

        res.fail(function (XMLHttpRequest, textStatus, errorThrown) {

          $scope.$apply(function(){
            $rootScope.loading= false;
            $rootScope.message= 'حدث خطأ اثناء الاتصال بالخادم .. من فضلك حاول مرة أخري';
            $('#failPopup').modal('show');
          })
        })


      }
///////////////////////////////////////////////////////////////////////////////

      $scope.setCurrentDate = function(){
        $scope.program.day = $scope.myDayIndex+1;
      }

      $scope.add_update_ProgramFun = function () {


        if($scope.program.id > 0 ) {
          $scope.updateProgramFun();
        }else{

          $scope.addProgramFun();
        }



      };

      $scope.addProgramFun = function () {

          $rootScope.loading= true;

          var res = '';

          var temp = {
            programId:$scope.program.id.toString(),
            hamlaId:$scope.hamlaId.toString(),
            dayId:$scope.program.day.toString(),
            time:timeService.convertTo24Hour($scope.program.time),
            text:$scope.program.details,
            title:$scope.program.title,
            lat:$scope.program.lat,
            lng:$scope.program.long,
            responserName:$scope.program.name,
            responserPhone:$scope.program.phone,
            image: $scope.program.image


          }
          res = ScheduleService.addProgram(temp);

          res.success(function (data) {
            $rootScope.loading= false;

            if(data.hasOwnProperty('programId')){

              debugger
              $rootScope.message = 'تم اضافة برنامج جديد للحملة بنجاح';
              $('#successPopup').modal('show');


            }else{
              $rootScope.message= 'حدث خطأ اثناء الاتصال بالخادم .. من فضلك حاول مرة أخري';
              $('#failPopup').modal('show');
            }
          });
          res.error(function (err) {
            $rootScope.loading= false;
            $rootScope.message = 'حدث خطأ اثناء الاتصال بالخادم .. من فضلك حاول مرة أخري';
            $('#failPopup').modal('show');
          });


      }




      $scope.updateProgramFun = function () {

        $rootScope.loading= true;

        var res = '';

          var temp = {
            programId:$scope.program.id.toString(),
            dayId:$scope.program.day.toString(),
            text:$scope.program.details,
            time: timeService.convertTo24Hour($scope.program.time),
            title:$scope.program.title,
            lat:$scope.program.lat.toString(),
            lng:$scope.program.long.toString(),
            responserName:$scope.program.name,
            responserPhone:$scope.program.phone,
            image: $scope.program.image
          }
          res = ScheduleService.updateProgram(temp);


          res.success(function (data) {
            $rootScope.loading= false;

            if(data.hasOwnProperty('result')){
              if(data.result){
                debugger
                $rootScope.message = 'تم تعديل البرنامج بنجاح';
                $('#successPopup').modal('show');
              }else{
                $rootScope.message= 'حدث خطأ اثناء الاتصال بالخادم .. من فضلك حاول مرة أخري';
                $('#failPopup').modal('show');
              }


            }else{
              $rootScope.message= 'حدث خطأ اثناء الاتصال بالخادم .. من فضلك حاول مرة أخري';
              $('#failPopup').modal('show');
            }
          });
          res.error(function (err) {
            $rootScope.loading= false;
            $rootScope.message = 'حدث خطأ اثناء الاتصال بالخادم .. من فضلك حاول مرة أخري';
            $('#failPopup').modal('show');
          });

      }

      $scope.textAreaAdjust = function(o) {
        o.style.height = "1px";
        o.style.height = (o.scrollHeight)+"px";
       // $('.textareaContainer').css('padding','5px 28px');
      }




      $scope.timeValidation = function(){

        $scope.program.time = $('.time-picker input').val();
        $('.time-picker .container').remove('hasError');
        $('.time-picker .container').remove('noError');
        $('.timepickerError').hide();
        if($scope.program.time != '')
        {

          if($scope.program.time.match(/^(0?[1-9]|1[012])(:[0-5]\d) [APap][mM]$/)) {
            $('.time-picker .container').addClass('noError');
            $('.timepickerError').hide();
            $scope.program.validTime = true;
          }else{
            $('.time-picker .container').addClass('hasError');
            $('.timepickerError').show();
            $scope.program.validTime = false;
          }
        }else{
          $('.time-picker .container').addClass('hasError');
          $('.timepickerError').show();
          $scope.program.validTime = false;
        }

      }








    }]);
