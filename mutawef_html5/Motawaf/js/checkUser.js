function getUrlVars() {

    var vars = {};
    var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
        vars[key] = value;
    });
    return vars;
}
function loading(){
  //  alert('مراجعة البيانات')
    var device_id =decodeURI(getUrlVars()["device_id"]).split("+").join(" ");
    var device_type = decodeURI(getUrlVars()["device_type"]).split("+").join(" ");
    var current_day = decodeURI(getUrlVars()["current_day"]).split("+").join(" ");

    localStorage.setItem('device_id', device_id);
    localStorage.setItem('device_type', device_type);
    localStorage.setItem('current_day', current_day);

    //alert("device_id  "+ localStorage.getItem('device_id'))
    //alert("device_type  "+localStorage.getItem('device_type'))
    //alert("current_day  "+localStorage.getItem('current_day'))

    if(localStorage.getItem('first_use') == null || localStorage.getItem('first_use') == ''){

       window.location.href = 'password.html';
    }else{
        window.location.href = 'hamalat.html';

    }

}


$(function(){

    window.addEventListener('load', function(e) {
        window.applicationCache.addEventListener('updateready', function(e) {
            if (window.applicationCache.status == window.applicationCache.UPDATEREADY) {

                console.log('_____________________________________________')
                console.log('omnia***updateready')
                window.applicationCache.update();
                window.applicationCache.swapCache();

            }
        }, false);
        window.applicationCache.addEventListener('noupdate', function(e){
            // start your app here
            console.log('_____________________________________________')
            console.log('omnia*** noupdate')
        }, false);
        window.applicationCache.addEventListener('cached', function(e){
            // start your app here
            console.log('_____________________________________________')
            console.log('omnia*** cached')
        }, false);

    }, false);

    window.applicationCache.addEventListener('error', function(e){
        console.log('_____________________________________________')
        console.log('omnia*** error')
        console.log('omnia*** '+arguments);
        loading();
    }, false);

    window.applicationCache.addEventListener('error', function(e){

        console.log('_____________________________________________')
        console.log('omnia*** error')
        console.log('omnia*** '+e);
        console.log('omnia*** '+e.reason);
        if (e.reason === 'manifest') {
            // start your app here
            console.log('_____________________________________________')
            console.log('omnia*** error')
            console.log('omnia*** manifest');
            loading();
        }
    }, false);
    loading();

});