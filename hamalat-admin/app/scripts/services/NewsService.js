hamalatAdminApp
  .factory('NewsService',['$http', '$rootScope',function($http, $rootScope) {

    return{
      addParagraph : function(info) {
        return $http.post($rootScope.domain+'HamlaNews/create', info);
      } ,

      updateParagraph : function(info) {
        return $http.post($rootScope.domain+'HamlaNews/edit', info);

      },
      removeParagraph : function(paragraphId){
        return $http.post($rootScope.domain+'HamlaNews/delete', paragraphId);
      },
      viewNews : function(info){
        return $http.post($rootScope.domain+'HamlaNews/Index', info);
      }
    };



  }]);
