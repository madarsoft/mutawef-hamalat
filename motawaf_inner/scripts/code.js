


var newsId = '';
var newsDetails = '';
var isMobile = true;
var shareUrl = '';
var shorlSharUrl ='';
var twitterTitle = '';
var iphoneDevice = false;
var androidDevice = false;
var app= false;


var shareCount  = 0;
var likeCount  =0 ;
var commentCount = 0 ;
var likeId = 0;
var userImage= '';
var userId= 0 ;
var userName= '';
var offset = 3600;
var currentOperation = 0 ;



function getUrlVars() {

    var vars = {};
    var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
        vars[key] = value;
    });
    return vars;
}

function init() {


    newsId = decodeURI(getUrlVars()["id"]).split("+").join(" ");

    newsDetails = {
        newsId :newsId,
        title:'',
        abbrev:'',
        content:'',
        main_image:'',
        date:''

    }
    offset = new Date().getTimezoneOffset();
    if(offset == 60)
    {
        offset = offset * 60;
    }else{
        offset = offset * -60;
    }

    //
    $('#comments').attr('src', 'https://api.mutawef.com/benefitsapi/GetComments?id='+newsDetails.newsId);

    $('head').append('<script async custom-element="amp-list" src="https://cdn.ampproject.org/v0/amp-list-0.1.js"></script>')
    $('head').append('<script async custom-template="amp-mustache" src="https://cdn.ampproject.org/v0/amp-mustache-0.1.js"></script>')

    shareUrl = 'http://mutawef.com/share/share/index?id=' + newsDetails.newsId
    shorlSharUrl = shareUrl;


    if(navigator.userAgent.match(/Android/) || navigator.userAgent.match(/iPhone/) || navigator.userAgent.match(/iPad/) || navigator.userAgent.match(/iPod/))
    {

        isMobile = true;

    }

    else{

        isMobile = false;
    }
    if( !isMobile){
      
    }


    if ( navigator.userAgent.match(/iPhone/) || navigator.userAgent.match(/iPad/) || navigator.userAgent.match(/iPod/)) {
        iphoneDevice = true;
        iOSversion();

    }else if(navigator.userAgent.match(/Android/) ) {
        androidDevice = true;
        $('.iframeContainer').css('text-align', 'justify');
    }
    app = true;

}

function iOSversion() {
    var str = navigator.userAgent;
    var n =  str.indexOf("OS");
    n = n + 3;
    str= str.substring(n , str.length)
    str= str.substr(0,str.indexOf(' ')); // "72"
    str= str.substr(str.indexOf(' ')+1);
    str = str.split('_');

    if(parseInt(str[0]) !=10 ||  parseInt(str[1]) !=3 )
    {

        $('.iframeContainer').css('text-align', 'justify');

    }
}


$('#newsImage').error(function(){
    $('#newsImage').unbind( "click" );
});

//$(window).scroll(function() {
//    var w= $(window).scrollTop()
//    var h =  $(window).height()
//    var d = $(document).height()
//
//    if($(window).scrollTop() + $(window).height() == $(document).height()) {
//        alert('ff')
//        $('#comments').attr('src','')
//
//        $('#comments').attr('src', '//admin.almosaly.com/comment/getComments?commentId=28&articleId='+newsDetails.newsId+'&count=5&timeOffset='+offset);
//
//    }
//});

window.loadSocialData = function(userNameParm, userIdParm, userImageParm, shareCountParm, likeCountParm, commentCountParm, likeIdParm ){


    shareCount  = shareCountParm;
    likeCount  = likeCountParm ;
    commentCount = commentCountParm ;
    likeId = likeIdParm;
    userImage= userImageParm;
    userId = userIdParm  ;
    userName= userNameParm;

    if(likeId != 0)
    {
        $('#like i').addClass('active');
    }
    $('#like span').html(likeCount);
    $('#comment span').html(commentCount);
    $('#share span').html(shareCount);

    if(userId !=0 ){
        $('#userImage img').attr('src', userImage)

    }
	selectOperation(currentOperation);
}


function scrollToComments() {
    $('html,body').animate({
            scrollTop: $("#myAppFB" ).offset().top -40
        },
        'slow');
}

function scrollToShare() {
    $('html,body').animate({
            scrollTop: $(".shareThisNewsWrapper" ).offset().top -40
        },
        'slow');
}

function selectOperation(operation){
	currentOperation = operation;
	
	if(operation != 0){
		 if(userId == 0 ){
			window.location.href = 'loginAction://'
		}else{
			 switch (operation) {
			  case 1:
			   add_remove_like();    //add or remove like for post
				break;
			  case 2:
				addComment(); //add  comment for post
				break;
			 
			 
			}
		}
	}
}

$('#comment').click(function(){
    scrollToComments();
})
$('#share').click(function(){
    scrollToShare();
})

function add_remove_like(){

	 if(likeId == 0)
    {
        addLike();
    }else{
        unLike();
    }
}
function addLike() {

    var data = {

        benefitId:userId,
        ClientId:newsDetails.newsId
    }

    $('#like i').hide();
    $('#likeLoad').show();


    var res = $.ajax({
        url: 'http://api.mutawef.com/Benefitsapi/like',
        type: "POST",
        data: JSON.stringify(data),
        datatype: 'json',
        contentType: false,
        processData: false


    })
    res.done(function (data, status, headers, config) {

        $('#likeLoad').hide();
        if (data.hasOwnProperty('result')) {
            if (data.result == true) {
                likeId = 1;
                likeCount++;
                $('#like i').addClass('active');
                $('#like i').show();
                $('#like span').html(likeCount);


            } else {
                $('#like i').show();

            }
        }
        else {
            $('#like i').show();

        }

    });
    res.fail(function (data, status, headers, config) {

        $('#likeLoad').hide();
        $('#like i').show();


    });

}

function unLike() {

    var data = {
        benefitId:userId,
        ClientId:newsDetails.newsId
    }

    $('#like i').hide();
    $('#likeLoad').show();


    var res = $.ajax({
        url: 'http://api.mutawef.com/Benefitsapi/DisLike',
        type: "POST",
        data: JSON.stringify(data),
        datatype: 'json',
        contentType: false,
        processData: false


    })
    res.done(function (data, status, headers, config) {

        $('#likeLoad').hide();
        if (data.hasOwnProperty('result')) {
            if (data.result == true) {
                likeCount--;
                likeId = 0;
                $('#like i').removeClass('active');
                $('#like i').show();
                $('#like span').html(likeCount);
            } else {

                $('#like i').show();

            }
        }else {

            $('#like i').show();

        }


    });
    res.fail(function (data, status, headers, config) {

        $('#likeLoad').hide();
        $('#like i').show();

    });

}



function addComment(){

   
	var text = $('#commentValue').val();
	if(text != '' ){
		sendComment(text);
	}


}
function sendComment(comment){
    var data = {
        benefitId: userId,
        ClientId: newsDetails.newsId,
        comment: comment
    }
    $('#sendComment').hide();
    $('#commentLoad').show();


    var res = $.ajax({
        url: 'http://api.mutawef.com/Benefitsapi/comment',
        type: "POST",
        data: JSON.stringify(data),
        datatype: 'json',
        contentType: false,
        processData: false


    })
    res.done(function (data, status, headers, config) {

        $('#commentLoad').hide();
            if (data.hasOwnProperty('commentId')) {
                commentCount++;

                $('#sendComment').show();
                $('#commentValue').val('');
                $('#comment span').html(commentCount);

                var item =
                    '<div role="listitem">'+
                        '<amp-img  src="'+userImage+'" width="1.1" height="1" layout="responsive"></amp-img>'+
                        '<div class="commentTextContent">'+
                            '<p class="userName">'+userName+'</p>'+
                            '<p class="userComment">'+comment+'</p>'+
                            '<span>'+
                                'الآن'+
                            '</span>'+
                        '</div>'+
                    '</div>';
                var oldH = $('#comments').height();
                var itemH = $('#comments div[role="listitem"]').height();
                $('#comments').height(oldH+ itemH);
                $('#comments div[role="list"]').prepend(item)


            } else {

                $('#sendComment').show();

            }


    });
    res.fail(function (data, status, headers, config) {

        $('#commentLoad').hide();
        $('#sendComment').show();

    });

}


function facebookShare(){

    window.open("http://www.facebook.com/sharer.php?u="+encodeURIComponent(shareUrl),'_system', 'location=no');


}
function twitterShare() {

    var twitterTxt =newsDetails.title + '\r\n' +shorlSharUrl;
    twitterTxt=   encodeURIComponent(twitterTxt);
//http://stackoverflow.com/questions/23095906/how-to-open-twitter-and-facebook-app-with-phonegap
//http://wiki.akosma.com/IPhone_URL_Schemes#Twitter
    // If Mac//


    if (iphoneDevice && app) {
        setTimeout(function () {
                window.open('https://itunes.apple.com/ca/app/twitter/id333903271?mt=8', '_system', 'location=no'); }
            ,25);
        window.open('twitter://post?message=' + twitterTxt, '_system', 'location=no');

    }

//If Android

    else {

        //window.open( 'https://twitter.com/intent/tweet?url='+twitterTxt+'&text='+$scope.twitterTitle , 'href','popup');
        window.open('https://twitter.com/share?url='+escape(shorlSharUrl)+'&text='+'\r\n' +encodeURIComponent(newsDetails.title )+'\r\n'  , '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,width=600');
        return false;
        return false;
    }
}




function whatsappShare(){
    //if (androidDevice && app) {
    //    // window.location.href = 'whatsappShare://';
    //}else{
        var whatsappTxt =newsDetails.title + '\r\n' + newsDetails.abbrev + '\r\n' + shorlSharUrl +'\r\n'+'تم نشر هذة المقالة من تطبيق المطوف';

        whatsappTxt=   encodeURIComponent(whatsappTxt);
        window.open('whatsapp://send?text='+ whatsappTxt , 'href', 'popup');
        return false;
    //}

}
function mailShare(){
    //if (androidDevice && app) {
    //    // window.location.href = 'mailShare://';
    //}else {
        var subject = newsDetails.title;
        var emailBody ='<p>'+newsDetails.title +'</p>'+ '\r\n' + '<p>'+newsDetails.abbrev +'</p>'+'\r\n' +'\r\n' +shorlSharUrl + '\r\n' +'\r\n'+'<img src="'+newsDetails.main_image +'" width="100%">'+ '\r\n'+'تم نشر هذة المقالة من تطبيق المطوف';
        emailBody=   encodeURIComponent(emailBody);
        window.open('mailto: ?subject=' + subject + '&body=' + emailBody, 'href', 'popup');
        return false;
   // }
}


function telegramShare(){
    //if (androidDevice && app) {
    //    // window.location.href = 'telegramShare://';
    //}else {
        var telegramTxt =newsDetails.title + '\r\n' + newsDetails.abbrev + '\r\n' + shorlSharUrl +'\r\n'+ 'تم نشر هذة المقالة من تطبيق المطوف';
        telegramTxt=   encodeURIComponent(telegramTxt);
        window.open('tg://msg?text=' +telegramTxt, 'href', 'popup');
        return false;
   // }
}


function textAreaAdjust(o) {
    o.style.height = "1px";
    o.style.height = (25+o.scrollHeight)+"px";
}